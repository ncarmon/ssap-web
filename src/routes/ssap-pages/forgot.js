import React, {PropTypes} from 'react'
import { Button, Row, Col, Form, Input,Select,message} from 'antd'
import { CountryList } from './country-list'
import {Link} from 'dva/router'
import { config,util,request,auth,lang } from '../../utils'
import styles from './forgot.less'
const Option=Select.Option;
const FormItem = Form.Item;

// let countryList=[];
// util.getAllCountryList().then(function(){
//   countryList=data.responseData;
// });  
const forgotPass = ({
  loginButtonLoading,
  changeSignUp,
  countryList,
  showLogin,
  forgotPassword,
  form: {
    getFieldDecorator,
    validateFieldsAndScroll
  }
}) => {
  function handleShowLogin(){
    showLogin();
  }
  function handleOk () {
    validateFieldsAndScroll((errors, formData) => {
      if (errors) {
        return
      }
      forgotPassword({
        username: '+'+formData.country+"-"+formData.mobile,
      })
    })
  }
  return (
    <div className={styles.form}>
      <div className={styles.logo}>
        {/* <img src={config.logoSrc} /> */}
        <span>{lang.tr('Forgot Password')}</span>
      </div>
      <form>
      <FormItem hasFeedback>
          {getFieldDecorator('country', {
            rules: [
              {
                required: true,
                message: lang.tr('Please select country')
              }
            ]
          })(<Select notFoundContent='Not Found' showSearch optionFilterProp={'desc'} placeholder={lang.tr("Please select country")}>
          {countryList.map((row, i) =>
              <Option key={'login'+i} desc={row.country_name.toLowerCase()+' '+row.country_name+' +'+row.calling_code} value={row.calling_code+''}>{row.country_name} (+{row.calling_code})</Option>
          )}
          </Select>)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: lang.tr('Please enter mobile number')
              }
            ]
          })(<Input type="number"  onPressEnter={handleOk} placeholder={lang.tr('Mobile No')} />)}
        </FormItem>
        <Row>
          <Col>
              <Button type='primary'  onClick={handleOk} loading={loginButtonLoading}>
                {lang.tr('Get Temporary Password')}
              </Button>
            </Col>
          <Col>
                <Button  type='default'  onClick={handleShowLogin}>
                {lang.tr('Click here to login')}
                </Button>
              {/* </Link> */}
            </Col>
        </Row>
      </form>
    </div>
  )
}

forgotPass.propTypes = {
  changeSignUp:PropTypes.func,
  form: PropTypes.object,
  loginButtonLoading: PropTypes.bool,
  onOk: PropTypes.func
}

export default Form.create()(forgotPass)
