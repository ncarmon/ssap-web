import React from 'react'
import {} from 'antd'
import { config,util } from '../../utils'
import {Select} from 'antd'
const Option=Select.Option;

let countryList=[];
        util.getAllCountryList().then(function(data){
        countryList=data.responseData;
});    

const CountryList = () => <Select showSearch optionFilterProp={'desc'} placeholder="Please select a country">
{countryList.map((row, i) =>
    <Option key={i} desc={row.country_name+' '+row.calling_code} value={row.calling_code+''}>{row.country_name} (+{row.calling_code})</Option>
)}
</Select>

export default {
    CountryList:CountryList
}
