import { create, remove, update, query } from '../services/contents'
import { parse } from 'qs'
import {propertyToUrl, urlToProperty, urlToList,extract} from "query-string";

export default {

  namespace: 'contents',

  state: {
    list: [],
    loading: false,
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    pagination: {
      showSizeChanger: true,
      showQuickJumper: true,
      showTotal: total => `Total ${total} Record`,
      current: 1,
      total: null
    }
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/contents') {
          dispatch({
            type: 'query',
            payload: location.query
          })
        }
      })
    }
  },

  effects: {
    *query ({ payload }, { call, put }) {
      yield put({ type: 'showLoading' })
      const data = yield call(query, parse(payload))
      if (data && data.responseData) {
        yield put({
          type: 'querySuccess',
          payload: data.responseData
        })
      }else{
        yield put({
          type: 'queryFail',
          payload: {}
        })
      }
    },
    *'delete' ({ payload }, { call, put }) {
      yield put({ type: 'showLoading' })
      const data = yield call(remove, payload)
      if (data && data.responseData) {
        yield put({
          type: 'query',
          payload: extract(location.hash)
        })
      }
    },
    *create ({ payload }, { call, put }) {
      yield put({ type: 'hideModal' })
      yield put({ type: 'showLoading' })
      const data = yield call(query, payload)
      if (data && data.responseData) {
        yield put({
          type: 'query',
          payload: {}
        })
      }
    },
    *update ({ payload }, { select, call, put }) {
      yield put({ type: 'hideModal' })
      // yield put({ type: 'showLoading' })
      // const id = yield select(({ users }) => users.currentItem.id)
      // const newUser = { ...payload, id }
      // const data = yield call(update, newUser)
      // if (data && data.responseData) {
      //   yield put({
      //     type: 'querySuccess',
      //     payload: extract(location.hash)
      //   })
      // }
    }
  },

  reducers: {
    showLoading (state) {
      return { ...state, loading: true }
    },
    hideLoading (state) {
      return { ...state, loading: true }
    },
    queryFail (state, action) {
      return { ...state,
          loading: false,
        }
    },
    querySuccess (state, action) {
      const {list, pagination} = action.payload
      return { ...state,
        list,
        loading: false,
        pagination: {
          ...state.pagination,
          ...pagination
        }}
    },
    showModal (state, action) {
      return { ...state, ...action.payload, modalVisible: true }
    },
    hideModal (state) {
      return { ...state, modalVisible: false }
    }
  }

}
