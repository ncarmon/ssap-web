import { create, remove, update, query,get } from '../services/campaigns'
import { parse } from 'qs'
import {propertyToUrl, urlToProperty, urlToList,extract} from "query-string";
//import message from '../routes/script/message';
import {message} from 'antd';

export default {

  namespace: 'campaigns',

  state: {
    list: [],
    loading: false,
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    pagination: {
      showSizeChanger: true,
      showQuickJumper: true,
      showTotal: total => `Total ${total} Record`,
      current: 1,
      total: null
    }
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/campaigns') {
          dispatch({
            type: 'query',
            payload: location.query
          })
        }
      })
    }
  },

  effects: {
    *query ({ payload }, { call, put }) {
      yield put({ type: 'showLoading' })
      let data;
      try{
        data = yield call(query, parse(payload))
      }catch(e){
        //message.error(e.message);
      }
      if (data && data.responseData) {
        yield put({
          type: 'querySuccess',
          payload: data.responseData
        })
      }else{
        yield put({
          type: 'queryFail',
          payload: {}
        })
      }
    },
    *'delete' ({ payload }, { call, put }) {
      yield put({ type: 'showLoading' })
      let data;
      try{
        data = yield call(remove, payload)
      }catch(e){

      }
      if (data && data.responseData) {
        yield put({
          type: 'query',
          payload: extract(location.hash)
        })
      }else{
        if(data && data.errors && data.errors.length){
          message.error(data.errors[0].message);
        }
        yield put({ type: 'queryFail' })
      }
    },
    *create ({ payload }, { call, put }) {
      yield put({ type: 'hideModal' })
      yield put({ type: 'showLoading' })
      const data = yield call(create, payload)
      if (data && data.responseData) {
        if(data.responseData.payment_url){
          return window.location=data.responseData.payment_url;
        }
        yield put({
          type: 'query',
          payload: {}
        })
      }else{
        if(data && data.errors && data.errors.length){
          message.error(data.errors[0].message);
        }
        yield put({ type: 'queryFail' })
      }
    },
    *get ({ payload }, { select, call, put }) {
      yield put({ type: 'showLoading' })
      const data = yield call(get,{campaign_id:payload.currentItem._id})
      yield put({ type: 'hideLoading' })
      if (data && data.responseData) {
        yield put({
          type: 'showModal',
          payload: data.responseData
        })
      }else{
        if(data && data.errors && data.errors.length){
          message.error(data.errors[0].message);
        }
        yield put({
          type: 'queryFail'
        })
        
      }
    },
    *update ({ payload }, { select, call, put }) {
      yield put({ type: 'hideModal' })
      yield put({ type: 'showLoading' })
      const id = yield select(({ users }) => users.currentItem.id)
      const newUser = { ...payload, id }
      const data = yield call(update, newUser)
      if (data && data.responseData) {
        yield put({
          type: 'querySuccess',
          payload: extract(location.hash)
        })
      }else{
        if(data && data.errors && data.errors.length){
          message.error(data.errors[0].message);
        }
        yield put({ type: 'queryFail' })
      }
    }
  },

  reducers: {
    showLoading (state) {
      return { ...state, loading: true }
    },
    hideLoading (state) {
      return { ...state, loading: false }
    },
    queryFail (state, action) {
      return { ...state,
          loading: false,
        }
    },
    querySuccess (state, action) {
      const {list, pagination} = action.payload
      return { ...state,
        list,
        loading: false,
        pagination: {
          ...state.pagination,
          ...pagination
        }}
    },
    showModal (state, action) {
      return { ...state, currentItem:action.payload, modalVisible: true }
    },
    hideModal (state) {
      return { ...state, modalVisible: false }
    }
  }

}
