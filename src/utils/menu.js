module.exports = [
    {
        key: 'dashboard',
        name: 'Dashboard',
        icon: 'pie-chart',
    },
    {
        key: 'contents',
        name: 'Contents',
        icon: 'picture',
    },
    {
        key: 'campaigns',
        name: 'Campaigns',
        icon: 'flag',
    },
    {
        key: 'profile',
        name: 'Profile',
        icon: 'user',
    }

]
