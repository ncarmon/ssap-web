import React, {PropTypes} from 'react'
import moment from 'moment'
import {config} from '../../utils'

class ShowDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: props.date || '',
        };
    }
    render() {
        return <span>&nbsp;{moment(this.state.date).format(config.displayDateFormat)}&nbsp;</span>
    }
}
export default ShowDate