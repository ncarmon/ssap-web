import React, {PropTypes} from 'react'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  Steps,
  LocaleProvider,
  message
} from 'antd';
import {lang} from '../../../utils'
import { Image } from 'semantic-ui-react'
import NewCampaign from './form'
// import "video-react/styles/scss/video-react.scss"; // import css



import enUS from 'antd/lib/locale-provider/en_US';
const FormItem = Form.Item
const Step = Steps.Step;
import UploadFile from '../upload'

const formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 14
  }
}

const modal = ({
  visible,
  type,
  item,
  onOk,
  onCancel,
  form,
  form:{
    validateFields,
    getFieldsValue
  }
}) => {
  let current=0;
  function handleOk() {
    validateFields((errors) => {
      if (errors) {
        for(let key in errors){
          message.error(errors[key].errors[0].message);
        }
        return
      }
      let data=getFieldsValue();
      let screensContents=[];
      data.selected_screens
      .forEach(screen => {
        let s={
          screen_id:screen._id,
          weeks:[]
        }
        screen.availability.forEach(week => {
          s.weeks.push({
            start_date:week.start_date,
            end_date:week.end_date,
            contents:data.content.map(function(content){
              return {
                content_id:content.content._id || content.content.content_id,
                repeat_pulse_count:content.pulse
              }
            })
          })
        });
        screensContents.push(s);
      });
      onOk({
        _id:item._id,
        title:data.campaign_name,
        start_date: data.start_date,
        end_date: data.end_date,
        screens:screensContents
      })
    })
  }
  const modalOpts = {
    title: `${type === 'create'
      ? lang.tr('Build Campaign')
      : lang.tr('Update Campaign')}`,
    visible,
    width:'95%',
    onOk: handleOk,
    onCancel,
    footer:null,
    wrapClassName: 'vertical-center-modal'
  }
  const FormOpts={
    form,
    item,
    onOk: handleOk,
  }
  return (
    <LocaleProvider locale={enUS}>
      <Modal {...modalOpts}>
        <NewCampaign {...FormOpts}/>
      </Modal>
    </LocaleProvider>
  )
}

modal.propTypes = {
  visible: PropTypes.any,
  form: PropTypes.object,
  item: PropTypes.object,
  onOk: PropTypes.func,
  onCancel: PropTypes.func
}

export default Form.create()(modal)
