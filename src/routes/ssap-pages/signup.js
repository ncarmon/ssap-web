import React, {PropTypes} from 'react'
import { Button, Row, Col, Form, Input,Select } from 'antd'
import {Link} from 'dva/router'
import { config,request,util,lang } from '../../utils'
import { CountryList } from './country-list'
import styles from './signup.less'
import { style } from 'd3-selection';
const Option=Select.Option;
const FormItem = Form.Item
const signup = ({
  loginButtonLoading,
  countryList,
  showLogin,
  onOkSignup,
  form: {
    getFieldsValue,
    getFieldDecorator,
    validateFields,
    validateFieldsAndScroll
  }
}) => {
  function handleShowLogin(){
    showLogin();
  }
  function handleOk () {
    validateFieldsAndScroll((errors, formData) => {
      if (errors) {
        return
      }
      onOkSignup({
        username: '+'+formData.country+"-"+formData.mobile,
        password: formData.password,
        email: formData.email,
        first_name: formData.first_name,
        last_name: formData.last_name
      });
    })
  }
  function checkConfirm(rule, value, callback) {
    var formData=getFieldsValue();
    if(value===formData.password){
      callback();
    }else{
      callback(lang.tr("Password mismatch"));
    }
  }
  function checkConfirmOnPasscheck(rule, value, callback){
    var formData=getFieldsValue();
    if (value && formData.cpassword) {
      validateFields(['cpassword'], { force: true });
    }
    callback();
  }
  return (
    <div className={styles.form}>
      <div className={styles.logo}>
        {/* <img src={config.logoSrc} /> */}
        <span>{lang.tr("OSA Signup")}</span>
      </div>
      <form>
      <FormItem hasFeedback>
          {getFieldDecorator('country', {
            rules: [
              {
                required: true,
                message: lang.tr('Please select country')
              }
            ]
          })(<Select notFoundContent={lang.tr('Not Found')} showSearch optionFilterProp={'desc'} placeholder={lang.tr("Please select country")}>
          {countryList.map((row, i) =>
              <Option key={'signup'+i} desc={row.country_name.toLowerCase()+' '+row.country_name+' +'+row.calling_code} value={row.calling_code+''}>{row.country_name} (+{row.calling_code})</Option>
          )}
          </Select>)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: lang.tr('Please enter mobile number')
              }
            ]
          })(<Input  onPressEnter={handleOk} placeholder={lang.tr("Mobile No")} />)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('email', {
            rules: [
              {
                required: true,
                type:'email',
                message: lang.tr("Please enter valid email")
              }
            ]
          })(<Input  onPressEnter={handleOk} placeholder={lang.tr("Email")} />)}
        </FormItem>
        <Row className={styles.rowAntFormItem}>
          <Col md={11}>
          <FormItem hasFeedback>
            {getFieldDecorator('first_name', {
              rules: [
                {
                  required: true,
                  message: lang.tr("Enter First Name")
                }
              ]
            })(<Input  onPressEnter={handleOk} placeholder={lang.tr("First Name")} />)}
          </FormItem>
            </Col>
            <Col md={11} offset={2}>
              <FormItem hasFeedback>
                {getFieldDecorator('last_name', {
                  rules: [
                    {
                      required: true,
                      message: lang.tr("Enter Last Name")
                    }
                  ]
                })(<Input  onPressEnter={handleOk} placeholder={lang.tr("Last Name")} />)}
              </FormItem>
            </Col>
        </Row>        
        <FormItem hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: lang.tr("Please enter your password")
              },
              {
                message: lang.tr("Invalid confirm password"),
                validator: checkConfirmOnPasscheck
              }
            ]
          })(<Input  type='password' onPressEnter={handleOk} placeholder={lang.tr("Set Password")} />)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('cpassword', {
            rules: [
              {
                required: true,
                message: lang.tr("Enter confirm password"),
              },
              {
                message: lang.tr("Invalid confirm password"),
                validator: checkConfirm
              }
            ]
          })(<Input  type='password' onPressEnter={handleOk} placeholder={lang.tr("Confirm Password")} />)}
        </FormItem>
        <Row>
        <Col>
              <Button type='primary'  onClick={handleOk} loading={loginButtonLoading}>
              {lang.tr("Register")}
              </Button>
            </Col>
          <Col>
                <Button  type='default'  onClick={handleShowLogin}>
                {lang.tr("I've already account")}
                </Button>
              {/* </Link> */}
            </Col>
        </Row>
      </form>
    </div>
  )
}

signup.propTypes = {
  form: PropTypes.object,
  loginButtonLoading: PropTypes.bool,
  onOk: PropTypes.func
}

export default Form.create()(signup)
