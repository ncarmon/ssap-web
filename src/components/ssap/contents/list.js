import React from 'react'
import {Table, Dropdown, Button, Menu, Icon, Modal,LocaleProvider,Input} from 'antd';
import {TweenOneGroup} from 'rc-tween-one'
import {Label} from 'semantic-ui-react'
import {lang} from '../../../utils'
import styles from './list.less'
const confirm = Modal.confirm
import enUS from 'antd/lib/locale-provider/en_US';


class list extends React.Component {
  constructor (props) {
    super(props)
    this.enterAnim = [
      {
        opacity: 0,
        x: 30,
        backgroundColor: '#fffeee',
        duration: 0
      }, {
        height: 0,
        duration: 200,
        type: 'from',
        delay: 250,
        ease: 'easeOutQuad',
        onComplete: this.onEnd
      }, {
        opacity: 1,
        x: 0,
        duration: 250,
        ease: 'easeOutQuad'
      }, {
        delay: 1000,
        backgroundColor: '#fff'
      }
    ]
    this.leaveAnim = [
      {
        duration: 250,
        opacity: 0
      }, {
        height: 0,
        duration: 200,
        ease: 'easeOutQuad'
      }
    ]
    const {current} = this.props.pagination
    this.currentPage = current
    this.newPage = current
    this.state = {
      width: 800
    }
  }

 /**
   * Calculate & Update state of new dimensions
   */
  updateDimensions() {
    if (window.innerWidth < 1000) {
      this.setState({width: 850});
    } else if (window.innerWidth > 1000) {
      this.setState({width: 0});
    } else {
      let update_width = window.innerWidth - 100;
      this.setState({width: update_width});
    }
  }

  /**
   * Add event listener
   */
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  getBodyWrapper = (body) => {
    // Switch paging to remove animation
    if (this.currentPage !== this.newPage) {
      this.currentPage = this.newPage
      return body
    }
    return (
      <TweenOneGroup component='tbody' className={body.props.className} enter={this.enterAnim} leave={this.leaveAnim} appear={false}>
        {body.props.children}
      </TweenOneGroup>
    )
  }

  handleMenuClick = (record, e) => {
  const {onDeleteItem, onEditItem} = this.props
  if (e.key === '1') {
    onEditItem(record)
  } else if (e.key === '2') {
    confirm({
      title: lang.tr('Are you sure you want to delete this content, On going campaigns still uses this content?'),
      onOk () {
        onDeleteItem({content_id:record._id})
      }
    })
  }
}

  onEnd = (e) => {
    e.target.style.height = 'auto'
  }

  async pageChange (pagination,filter,sort) {
    await this.props.onPageChange(pagination,filter,sort)
    this.newPage = pagination.current
  }

  render () {
    const {
      loading,
      dataSource,
      pagination,
      onDeleteItem,
      onEditItem
    } = this.props

     const columns = [
    {
      title: lang.tr('Thumb'),
      dataIndex: 'thumb',
      key: 'thumb',
      width: 150,
      className: styles.listthumb,
      render: (url,record) => <div className={styles.listthumb} onClick={()=>{onEditItem(record)}} style={{cursor:'pointer','backgroundImage':'url('+url+')'}}></div>
    },{
      title: lang.tr('File Name'),
      dataIndex: 'file_name',
      style:{textAlign:'left'},
      key: 'file_name',
      sorter: true,
    },{
      title: lang.tr('Content Type'),
      dataIndex: 'content_type',
      key: 'content_type',
      //render: (content_type) =>lang.tr(content_type),
      width: 200,
      sorter: true,
    },{
      title: lang.tr('Size'),
      dataIndex: 'resolution',
      key: 'resolution',
      width: 200,
      sorter: true,
    }, {
      title: lang.tr('Status'),
      dataIndex: 'act1',
      width: 200,
      key: 'act1',
      filters: [
        { text: lang.tr('Approved'), value: 1 },
        { text: lang.tr('Rejected'), value: 0 },
        { text: lang.tr('Pending'), value: -1 },
      ],
      sorter: true,
     className: styles.label,
    render: (active) =>  active>=0?(active==0?
        <Label color='red' horizontal size={'mini'} pointing={'left'}>{lang.tr('Rejected')}</Label>:
        <Label  color='green' horizontal size={'mini'} pointing={'left'}>{lang.tr('Approved')}</Label>):
        <Label  color='orange' horizontal size={'mini'} pointing={'left'}>{lang.tr('Pending')}</Label>
    }, {
      title: lang.tr('Operation'),
      key: 'operation',
      width: 100,
      render: (text, record) => {
         return (<Dropdown overlay={<Menu onClick={this.handleMenuClick.bind(null, record)}>
           <Menu.Item key='1'>{lang.tr('View')}</Menu.Item>
           <Menu.Item key='2'>{lang.tr('Delete')}</Menu.Item>
         </Menu>}>
           <Button style={{ border: 'none' }}>
             <Icon style={{ marginRight: 2 }} type='bars' />
             <Icon type='down' />
           </Button>
         </Dropdown>)
       }
    }
  ]
   
    return <div>
      <LocaleProvider locale={enUS}>
      <Table bordered columns={columns} dataSource={dataSource} loading={loading} onChange={::this.pageChange} pagination={pagination} rowKey={record => record._id}  scroll={{ x: this.state.width,y:'350px' }} /></LocaleProvider>
    </div>
  }
}

export default list