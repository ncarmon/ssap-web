import React, {PropTypes} from 'react'
import moment from 'moment'
import {config,request,lang} from '../../../utils'
import ShowDate from '../ShowDate';
import {message,Card as AntdCard,Row,Col,Icon} from 'antd';
import { Card,Message,Statistic,Grid } from 'semantic-ui-react';
import styles from './dashboard.less'
import ContentModal from '../contents/modal'

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        };
    }
    componentDidMount() {
        this.getData();
    }

    componentWillUnmount() {
    }
    getData=()=>{
        let me=this;
        me.setState({...me.state,loading:true});
        request('dashboard/ongoing-campaigns',{},function(err,data){
            me.setState({...me.state,loading:false});
            if(err){
                return message.error(err.message);
            }
            me.setState({...me.state,list:data.list});
        })
    }
    render() {
        let me=this;
        let ShowContent=(content)=>{
            this.state.showModal=true;
            this.state.current_content=content;
            this.setState({...this.state});
          }
          const contentModalProps = {
            item: this.state.current_content,
            type: 'update',
            visible: this.state.showModal,
            onOk(data) {
                me.setState({...me.state,showModal:false})
            },
            onCancel() {
                me.setState({...me.state,showModal:false})
            }
          }
        const ContentModalGen = () => <ContentModal {...contentModalProps}/>
        const {loading}=this.state;
        return <div>
            <ContentModalGen/>
                <Card fluid color='blue'>
                    <Card.Content>
                        <Card.Header style={{fontSize: '18px'}}>{lang.tr('Ongoing Campaigns')}</Card.Header>
                        {
                            !loading && this.state.list.length==0 &&
                            <Card.Meta>{lang.tr('Currently there are no campaigns are runnig')}</Card.Meta>
                        }
                        <Card.Description style={{minHeight: '65vh'}}>
                        &nbsp;
                        {
                            loading &&
                            <AntdCard style={{ width: '100%' }} loading>&nbsp;</AntdCard>           
                        }
                        {
                            loading &&
                            <AntdCard style={{ width: '100%' }} loading>&nbsp;</AntdCard>           
                        }
                            {
                                !loading &&
                                <Card.Group>
                                        {
                                            this.state.list.map((campign,key)=>
                                                
                                                    <Card fluid color='green' key={key}>
                                                        <Card.Content>
                                                            <Card.Header><Icon type="flag" />{campign.campaign_title}</Card.Header>
                                                            <Card.Meta><Icon type="calendar" /><ShowDate date={campign.start_date}/> {lang.tr('to')} <ShowDate date={campign.end_date}/></Card.Meta>
                                                            <Card.Description>
                                                                <Card.Group>
                                                                    {
                                                                        campign.screens.map((screen,key)=>
                                                                        <Card fluid key={key}>
                                                                            {/* <Image src='/assets/image-text.png' /> */}
                                                                            <Card.Content>
                                                                            <Card.Header>{lang.tr('SCREEN')} {key+1} :<Icon type="desktop" /> {screen.resolution}</Card.Header>
                                                                            <Card.Meta><Icon type="environment" />{screen.address},  {screen.city}</Card.Meta>
                                                                            <Card.Description>
                                                                                <Card.Group itemsPerRow={4}>
                                                                                    {
                                                                                        screen.contents.map((content,key)=>
                                                                                            <Card size='mini' key={key}>
                                                                                                <Row style={{margin:0}}>
                                                                                                    <Col  span={12} onClick={()=>{ShowContent(content)}}>
                                                                                                        <div className={styles.dashboardContentThumb} style={{cursor:'pointer','backgroundImage':'url('+content.thumb+')'}}></div>
                                                                                                    </Col>
                                                                                                    <Col span={12}>
                                                                                                        <div className={styles.plusePosition}>
                                                                                                            {content.pulses} <div className={styles.plusePositionLbl}>{lang.tr('Pulse(s)')}</div>
                                                                                                        </div>
                                                                                                        <div className={styles.contentDateRange}>
                                                                                                            <ShowDate date={content.start_date}/> {lang.tr('to')} <ShowDate date={content.end_date}/>
                                                                                                        </div>
                                                                                                    </Col>
                                                                                                </Row>
                                                                                            </Card>
                                                                                        )
                                                                                    }
                                                                                </Card.Group>
                                                                            </Card.Description>
                                                                            </Card.Content>
                                                                            <Card.Content extra>
                                                                            {lang.tr('Total')} {screen.contents.length} {lang.tr('content(s) on this screen')}
                                                                            </Card.Content>
                                                                        </Card>
                                                                    )
                                                                    }
                                                                </Card.Group>
                                                            </Card.Description>
                                                        </Card.Content>
                                                        <Card.Content extra>
                                                        {lang.tr('Total')} {campign.screens.length} {lang.tr('screen(s)')}
                                                        </Card.Content>
                                                    </Card>
                                                
                                            )
                                        }
                                        </Card.Group>
                            }
                            
                        </Card.Description>
                    </Card.Content>
                </Card>
        </div>
    }
}
export default Dashboard