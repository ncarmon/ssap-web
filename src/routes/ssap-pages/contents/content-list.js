import React, {PropTypes} from 'react'
import {routerRedux} from 'dva/router'
import {connect} from 'dva'
import ContentList from '../../../components/ssap/contents/list'
import ContentSearch from '../../../components/ssap/contents/search'
import ContentModal from '../../../components/ssap/contents/modal'
import {Row, Col} from 'antd';

function Contents({location, dispatch, contents}) {
  const {
    loading,
    list,
    pagination,
    currentItem,
    modalVisible,
    modalType
  } = contents
  const {field, keyword} = location.query

  const userModalProps = {
    showLoading(){
      dispatch({type: 'contents/showLoading'})
    },
    hideLoading(){
      dispatch({type: 'contents/hideLoading'})
    },
    item: modalType === 'create'
      ? {}
      : currentItem,
    type: modalType,
    visible: modalVisible,
    onOk(data) {
      dispatch({type: `contents/${modalType}`, payload: data})
    },
    onCancel() {
      dispatch({type: 'contents/hideModal'})
    }
  }

  const userListProps = {
    dataSource: list,
    loading,
    pagination: pagination,
    onPageChange(page,filter,sort) {
      const {query, pathname} = location
      query.filter=null;
      dispatch(routerRedux.push({
        pathname: pathname,
        query: {
          ...query,
          page: page.current,
          sort_by:sort.field,
          filter:JSON.stringify(filter),
          sort_order:sort.order,
          pageSize: page.pageSize
        }
      }))
    },
    onDeleteItem(id) {
      dispatch({type: 'contents/delete', payload: id})
    },
    onEditItem(item) {
      dispatch({
        type: 'contents/showModal',
        payload: {
          modalType: 'update',
          currentItem: item
        }
      })
    }
  }

  const userSearchProps = {
    field,
    keyword,
    onSearch(fieldsValue) {
      fieldsValue.keyword.length
        ? dispatch(routerRedux.push({
          pathname: '/contents',
          query: {
            field: fieldsValue.field,
            keyword: fieldsValue.keyword
          }
        }))
        : dispatch(routerRedux.push({pathname: '/contents'}))
    },
    onAdd() {
      dispatch({
        type: 'contents/showModal',
        payload: {
          modalType: 'create'
        }
      })
    }
  }

  const ContentModalGen = () => <ContentModal {...userModalProps}/>
  

  return (
    <div className='content-inner'>
      <Row>
        <Col xs={24} md={24} lg={24}>
          <ContentSearch {...userSearchProps}/>
          <ContentList {...userListProps}/>
          <ContentModalGen/>
        </Col>

      </Row>

    </div>
  )
}

Contents.propTypes = {
  contents: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func
}

function mapStateToProps({contents}) {
  return {contents}
}

export default connect(mapStateToProps)(Contents)
