import React, {PropTypes} from 'react'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  LocaleProvider,
} from 'antd';
import {lang} from '../../../utils'
import { Image } from 'semantic-ui-react'
import { Player } from 'video-react';


import enUS from 'antd/lib/locale-provider/en_US';
import UploadFile from '../upload'
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6
  },
  wrapperCol: {
    span: 24
  }
}

const modal = ({
  visible,
  type,
  item = {},
  onOk,
  onCancel,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue
  }
}) => {
  function handleOk() {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key
      }
      onOk(data)
    })
  }

  const modalOpts = {
    title: `${type === 'create'
      ? lang.tr('Upload New Content')
      : lang.tr('View Content')}`,
    visible,
    //afterClose: handleOk,
    onCancel,
    okText:lang.tr('Close'),
    cancelText:'',
    destroyOnClose:true,
    footer:null,
    wrapClassName: 'vertical-center-modal'
  }

  return (
    <LocaleProvider locale={enUS}>
      <Modal {...modalOpts} style={{textAlign:"center"}}>
        {item._id?
        (item.content_type=='IMAGE'?
        <Image size='medium' bordered src={item.url} style={{margin:'auto'}} />:
        <Player
          preload
          preload="metadata"
          playsInline
          poster={item.thumb}
          src={item.url+'&stream=1'}
        />):
        <Form horizontal>
          <FormItem hasFeedback {...formItemLayout}>
            {getFieldDecorator('name', {
              initialValue: item.name,
              rules: [
                {
                  required: true,
                  message: lang.tr('File cannot be blank')
                }
              ]
            })(<UploadFile onComplete={()=>{handleOk();}}/>)}
          </FormItem>
        </Form>}
      </Modal>
    </LocaleProvider>
  )
}

modal.propTypes = {
  visible: PropTypes.any,
  form: PropTypes.object,
  item: PropTypes.object,
  onOk: PropTypes.func,
  onCancel: PropTypes.func
}

export default Form.create()(modal)
