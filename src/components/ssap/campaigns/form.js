import React from 'react'
import {Button, Modal, Icon,LocaleProvider, Tree,Row, Col, message,Form,Input,Steps,DatePicker,List,Tooltip,Tag} from 'antd'
import {TweenOneGroup} from 'rc-tween-one'
import moment from 'moment';
import {config,lang} from '../../../utils';
const confirm = Modal.confirm
import ScreenPicker from '../ScreenPicker';
import ContentPicker from '../ContentPicker';
import ContentPicker2 from '../ContentPicker2';

import {Card, Image,Header,Table} from 'semantic-ui-react';
import enUS from 'antd/lib/locale-provider/en_US';
import ShowDate from '../ShowDate';
import ShowAmount from '../ShowAmount';
import PickerStyle from '../ContentPicker.less';
import rate from '../../../routes/script/rate';
const FormItem = Form.Item
const Step = Steps.Step;

class NewCampaign extends React.Component {
    constructor(props) {
        super(props)
        var item=props.item || {}
        if(item._id){
            item.campaign_name=item.title;
            item.start_date=moment(item.start_date);
            item.end_date=moment(item.end_date);
        }
        item.content=item.content || [{pulse:1}]
        this.state = {
            current: 0,
            item: item
        }
    }

    componentDidMount() {
        if(this.state.item._id){
            let content=[],contentUnique={};
            let firstScreen=true;
            let firstWeek=true;
            this.state.item.selected_screens=this.state.item.screens.map(function(screen){
                screen.added=true;
                screen.availability=screen.weeks.map(function(row){
                    row.start_date=moment(row.start_date);
                    row.end_date=moment(row.end_date);
                    row.contents=row.contents.map(function(c){
                        if(firstScreen && firstWeek){
                            c._id=c._id || c.content_id;
                            content.push({content:c,pulse:c.repeat_pulse_count});
                            // if(!contentUnique[c._id]){
                            //     contentUnique[c._id]={content:c,pulse:c.repeat_pulse_count}
                            // }else{
                            //     contentUnique[c._id].pulse+=Number(c.repeat_pulse_count);
                            // }    
                        }
                        return {
                            content:c,
                            pulse:c.repeat_pulse_count
                        };
                    })
                    firstWeek=false;
                    return row
                })
                firstScreen=false;
                return screen;
            });
            // for(var key in contentUnique){
            //     if(contentUnique[key]){
            //         content.push(contentUnique[key]);
            //     }
            // }
            this.state.item=this.mergeSelectedScreens(this.state.item);
            this.state.item.content=content;
            this.setState({...this.state});
        }
    }

    componentWillUnmount() {
    }

    onEnd = (e) => {

    }
    calculateTotalTariff=(screenList)=>{
        let total=0;
        if(screenList && screenList.length){
            for(var i=0;i<screenList.length;i++){
                //total+=this.calculatePerScreenTariff(screenList[i]);
                total+=screenList[i].total_rate*this.countPules(this.state.item.content);
            }
            
        }
        return total;
    }
    getRateByDate=(screen,date)=>{
        let rate=0;
        if(screen && screen.tariffs && screen.tariffs.length){
            for(var i=0;i<screen.tariffs.length;i++){
                if(moment(screen.tariffs[i].date).format('YYYYMMDD')==moment(date).format('YYYYMMDD')){
                    rate=parseFloat(screen.tariffs[i].rate);
                }
            }
        }
        return rate;
    }
    calculatePerContent=(screen,week,index)=>{
        let total=0;
        let startDate=new Date(week.start_date);
        let endDate=new Date(week.end_date);
        let tableData=[];
        for(let day=new Date(startDate);day<=endDate;day.setDate(Number(day.getDate())+1)){
            let rate=this.getRateByDate(screen,day);
            const rowSum=(rate*week.contents[index].pulse)
            total+=rowSum;
            tableData.push({
                amount:rowSum,
                date:moment(day)
            });
        }
        
        //const ToolTipData=<Table columns={columns} dataSource={tableData} size="small" />
        return {
            total:total,
            list:tableData,
            //ToolTipData:ToolTipData
        }
    }
    calculatePerScreenTariff=(screen)=>{
        let total=0;
        if(screen && screen.availability){
            for(var i=0;i<screen.availability.length;i++){
               let week=screen.availability[i] ;
               let startDate=new Date(week.start_date);
               let endDate=new Date(week.end_date);
               for(let day=new Date(startDate);day<=endDate;day.setDate(Number(day.getDate())+1)){
                   let rate=this.getRateByDate(screen,day);
                   for(var c=0;c<week.contents.length;c++){
                        total+=(rate*week.contents[c].pulse)
                    }
               }
            }
        }
        return total;
    }
    countPules=(contentList)=>{
        let total=0;
        contentList.map(function(item){
            total+=item.pulse;
            return item;
        });
        return total;
    }
    isAllContentsActive=(contentList)=>{
        for(let i=0;i<contentList.length;i++){
            if(contentList[i].content.act1<=0){
                return false;
            }
        }
        return true;
    }
    render() {
        const formItemLayout = {
            labelCol: {
                span: 6
            },
            wrapperCol: {
                span: 14
            }
        }
        const formItemLayoutMap = {
            labelCol: {
                span: 0
            },
            wrapperCol: {
                span: 24
            }
        }
        const {
            loading,
            dataSource,
            pagination,
            onDeleteItem,
            onEditItem,
            item,
            form: {
                getFieldDecorator,
                validateFields,
                getFieldsValue
            }
        } = this.props
        let me = this;
        let stepFields = [['campaign_name'], ['start_date', 'end_date'], ['content'],['selected_screens']];
        this.handleNextStep = function () {
            validateFields(stepFields[me.state.current], (errors) => {
                if (errors) {
                    return
                }
                if (me.state.current < 4) {
                    let data = this.mergeSelectedScreens(getFieldsValue());
                    me.setState({...me.state, current: me.state.current + 1, item: {...me.state.item,...data}});
                }
            })

        }
        this.disabledStartDate = function (current) {
            let startDates=config.campaignStartDates || [0,3];
            return current && ((current < moment().endOf('day')) || startDates.indexOf(parseInt(moment(current).format('d')))<0);
        }
        this.disabledEndDate = function (current) {
            let data = getFieldsValue();
            let startDate=data.start_date;
            if(!startDate){
                return true;
            }
            let dayToEnd= moment(startDate).format('d');
            dayToEnd=dayToEnd-1;
            if(dayToEnd<0){
                dayToEnd+=7;
            }else if(dayToEnd>6){
                dayToEnd-=7;
            }
            return current && ((current < moment(startDate)) || !(moment(current).format('d')==dayToEnd) );
        }
        this.mergeSelectedScreens=function (data) {
            return data;
            if(!data.screens_with_contents){
                data.screens_with_contents=[];
            }
            if(!data.selected_screens){
                data.selected_screens=[];
            }
            for(var i=0;i<data.selected_screens.length;i++){
                let screen;
                for(var j=0;j<data.screens_with_contents.length;j++){
                    if(data.screens_with_contents[j]._id==data.selected_screens[i]._id){
                        screen=data.screens_with_contents[j];
                        break;
                    }
                }
                if(screen){
                    data.selected_screens[i]=screen;
                }

            }
            data.screens_with_contents=data.selected_screens;
            return data;
        }
        this.handlePrevStep = function () {
            if (this.state.current > 0) {
                let data = this.mergeSelectedScreens(getFieldsValue());
                me.setState({...me.state, current: me.state.current - 1, item: {...me.state.item,...data}});
            }
        }
        const AnyReactComponent = ({text}) => <div>{text}</div>;
        const mapProperties = {};
        this.updateContensOnScreen=function(selected_screens){
             let data=getFieldsValue();
            // if(!data.selected_screens){
            //     return;
            // }
            let found;
            //this.state.item=data;
            this.state.item.selected_screens=selected_screens || [];
            this.state.item.screens_with_contents=this.state.item.screens_with_contents || [];
            for(var j=0;j<this.state.item.selected_screens.length;j++){
                found=false;
                for(var i=0;i<this.state.item.screens_with_contents.length;i++){
                    if(this.state.item.selected_screens[j]._id==this.state.item.screens_with_contents[i]._id){
                        found=true;
                        break;
                    }
                }
                if(!found){
                    this.state.item.screens_with_contents.push(this.state.item.selected_screens[j]);
                }
            }
            for(var j=0;j<this.state.item.screens_with_contents.length;j++){
                found=false;
                for(var i=0;i<this.state.item.selected_screens.length;i++){
                    if(this.state.item.selected_screens[i]._id==this.state.item.screens_with_contents[j]._id){
                        found=true;
                        break;
                    }
                }
                if(!found){
                    this.state.item.screens_with_contents.splice(j,1);
                    j--;
                }
            }
            this.setState({...this.state});           
        
        }
        const PriceColumns=[
            {
                title: lang.tr('Date'),
                dataIndex: 'date',
            },
            {
                title: lang.tr('Rate'),
                dataIndex: 'amount',
            }
        ];
        const steps = [
            {
                title: lang.tr('Campaign Details'),
                content: <div style={{marginTop:'11%'}}>
                    <FormItem label={lang.tr("Campaign Title")} hasFeedback {...formItemLayout}>
                        {getFieldDecorator('campaign_name', {
                            initialValue: this.state.item.campaign_name,
                            rules: [
                                {
                                    required: true,
                                    message: lang.tr('Campaign title is required')
                                }
                            ]
                        })(<Input onKeyPress={(e)=>{
                            if (e.key === 'Enter') {
                                this.handleNextStep();
                            }
                        }} disabled={this.state.item.act1&&this.state.item.act1>0} placeholder={lang.tr("Campaign Title")} id="error"/>)}
                    </FormItem>
                </div>,
            }, {
                title: lang.tr('Schedule'),
                content: <div style={{marginTop:'10%'}}>
                    <FormItem label={lang.tr("Starts on")} hasFeedback {...formItemLayout}>
                        {getFieldDecorator('start_date', {
                            initialValue: this.state.item.start_date,
                            rules: [
                                {
                                    required: true,
                                    message: lang.tr('Start Date is required')
                                },
                                {
                                    required: true,
                                    message: lang.tr('Invalid start date'),
                                    validator:function (rule,val,cb) {
                                        var now=moment();
                                        if(item && item.act1>0){
                                            return cb();
                                        }
                                        if(val && val<now){
                                            return cb(lang.tr('Invalid start date'));
                                        }else{
                                            return cb();
                                        }
                                    }
                                },
                                function (rule,val,cb) {
                                    let data=getFieldsValue();
                                    if(val && data.end_date){
                                        validateFields(['end_date'], { force: true });
                                    }
                                    cb();
                                }
                            ]
                        })(<DatePicker placeholder={lang.tr('Select date')} disabled={this.state.item.act1&&this.state.item.act1>0} disabledDate={this.disabledStartDate}/>)}
                    </FormItem>
                    <FormItem label={lang.tr("Show till")} hasFeedback {...formItemLayout}>
                        {getFieldDecorator('end_date', {
                            initialValue: this.state.item.end_date,
                            rules: [
                                {
                                    required: true,
                                    message: lang.tr('End Date is required')
                                },
                                {
                                    required: true,
                                    message: lang.tr('End date is not valid'),
                                    validator:function (rule,val,cb) {
                                        if(item && item.act1>0){
                                            return cb();
                                        }
                                        let data=getFieldsValue();
                                        let diff=0;
                                        if(data.start_date && val){
                                            val.hour(23);
                                            val.minute(59);
                                            val.second(59);
                                            data.start_date.hour(0);
                                            data.start_date.minute(0);
                                            data.start_date.second(0);
                                            diff=val.diff(data.start_date, 'days')+1
                                        }
                                        if(!(diff%7==0) || diff<7){
                                            return cb(lang.tr('Invalid'));
                                        }else{
                                            return cb();
                                        }
                                    }
                                }
                            ]
                        })(<DatePicker placeholder={lang.tr('Select date')} disabled={this.state.item.act1&&this.state.item.act1>0} disabledDate={this.disabledEndDate} />)}
                    </FormItem>
                </div>,
            },{
                title: lang.tr('Select Content'),
                content:<div style={{marginTop:'1%'}}><FormItem label={lang.tr("Select Content")} hasFeedback {...formItemLayout}>
                    {getFieldDecorator('content', {
                        initialValue: this.state.item.content,
                        rules: [
                            {
                                message: lang.tr('Total pulse should not exceded more than 8 pules'),
                                validator:function (rule,value,cb) {
                                    var countPulse=0;
                                    for(var i=0;i<value.length;i++){
                                        if(value[i].pulse)
                                            countPulse+=value[i].pulse;    
                                    }
                                    if(countPulse>config.max_pulses){
                                        return cb(lang.tr('Total pulse should not exceded more than 8 pules'));
                                    }
                                    cb();
                                }
                            },
                            {
                                message: lang.tr('Select missing contents'),
                                validator:function (rule,value,cb) {
                                    if(!(value && value.length)){
                                        return cb(lang.tr('Select content'));
                                    }
                                    var countPulse=0;
                                    for(var i=0;i<value.length;i++){
                                        if(!(value[i] && value[i].pulse)){
                                            return cb(lang.tr('Select pulse'));
                                        }
                                        if(!(value[i] && value[i].content)){
                                            return cb(lang.tr('Select content'));
                                        }
                                        countPulse+=value[i].pulse;    
                                    }
                                    cb();
                                }
                            }
                        ]
                    })(<ContentPicker2 startDate={this.state.item.start_date}  endDate={this.state.item.end_date} disabled={this.state.item.act1&&this.state.item.act1>0}/>)}
                </FormItem></div>,
            }, {
                title: lang.tr('Select Screens'),
                content: <FormItem hasFeedback {...formItemLayoutMap}>
                    {getFieldDecorator('selected_screens', {
                        initialValue: this.state.item.selected_screens,
                        rules: [
                            {
                                message: lang.tr('Screens is required'),
                                validator:function (rule,value,cb) {
                                    if(!(value && value.length)){
                                        return cb(lang.tr('Screens is required'));
                                    }else if(value && value.length){
                                        var addCount=0;
                                        for(var i=0;i<value.length;i++){
                                            if(value[i].added){
                                                addCount++;
                                            }
                                        }
                                        if(addCount==0){
                                            return cb(lang.tr('Screens is required'));
                                        }
                                    }
                                    cb();
                                }
                            }
                        ]
                    })(<ScreenPicker content={this.state.item.content} disabled={this.state.item.act1&&this.state.item.act1>0} {...mapProperties} startDate={this.state.item.start_date}  endDate={this.state.item.end_date}  onChange={(screens)=>{this.updateContensOnScreen(screens)}}  />)}
                </FormItem>,
            }, {
                title: lang.tr('Payment'),
                content: <div>
                    <Row style={{marginTop: '10px'}}>
                        <Col span={24}>
                        <Card.Group>
                                          {(this.state.item.selected_screens || []).map((screen,key)=>
                                          <Card key={screen._id} style={{minHeight:'120px'}}>
                                                <Card.Content>
                                                    <div className={PickerStyle.screenThumb} style={{backgroundImage:'url('+(screen.screen_photo || '/assets/default-screen.jpg')+')'}}>
                                                        <div>
                                                            <div className={PickerStyle.pulses}>
                                                            <h2>{this.countPules(this.state.item.content)}</h2>
                                                             {lang.tr('pulse')}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <Card.Header className={PickerStyle.contentMargin}>
                                                        {screen.screen_name}({screen.resolution})
                                                        <Icon type='close-circle-o' onClick={() => {
                                                            screen.added=false;
                                                            this.state.item.selected_screens.splice(key,1);
                                                            this.setState({...this.state});
                                                            if(this.state.item.selected_screens.length==0){
                                                               this.handlePrevStep(); 
                                                            }
                                                        }} style={{
                                                            position: 'absolute',
                                                            right: '2px',
                                                            color: '#999',
                                                            cursor: 'pointer',
                                                            padding: '3px',
                                                            top: '2px'
                                                        }}/>
                                                    </Card.Header>
                                                    <Card.Meta className={PickerStyle.contentMargin}>
                                                        {screen.address}
                                                    </Card.Meta>
                                                    {/* <Card.Description className={PickerStyle.contentMargin}>
                                                        {(screen.tags || []).map((tag, key) =>
                                                            <Tag key={key} closable={false} color="blue">{tag}</Tag>
                                                        )} 
                                                    </Card.Description> */}
                                                    <Card.Description className={PickerStyle.contentMarginBottom}>
                                                        <ShowAmount amount={screen.total_rate*this.countPules(this.state.item.content)}/>
                                                    </Card.Description>
                                                </Card.Content>
                                            </Card>
                                          )}
                                        </Card.Group>
                        </Col>
                    </Row>
                </div>,
            }];
        
        return <Form layout={'horizontal'}>
            <Steps current={this.state.current}>
                {steps.map(item =>
                    <Step key={item.title} title={item.title}/>)
                }
            </Steps>
            <div className="steps-content">{steps[this.state.current].content}</div>
            <div className="steps-action">
                {
                    this.state.current > 0
                    &&
                    <Button style={{marginLeft: 8}} onClick={() => this.handlePrevStep()}>
                        {lang.tr('Previous')}
                    </Button>
                }
                {
                    (this.state.current === steps.length - 1 && this.calculateTotalTariff(this.state.item.screens_with_contents)>0)
                    &&
                    <Button type="primary" onClick={() => this.props.onOk()} disabled={this.state.item.act1&&this.state.item.act1>0}>
                        {
                            (this.state.item.act1&&this.state.item.act1==2)&&
                            <span>{lang.tr('Paid')}</span>
                        }
                        {
                            (this.state.item.act1!=2&&(!(this.state.item.act1&&this.state.item.act1==2)&&this.isAllContentsActive(this.state.item.content)))&&
                            <span>{lang.tr('Pay')}</span>
                        }
                        {
                            (this.state.item.act1!=2&&!(!(this.state.item.act1&&this.state.item.act1==2)&&this.isAllContentsActive(this.state.item.content)))&&
                            <span>{lang.tr('Get Approval & Pay Later')}</span>
                        } <ShowAmount amount={this.calculateTotalTariff(this.state.item.screens_with_contents)}/>
                    </Button>
                }
                {
                    (this.state.current === steps.length - 1 && this.calculateTotalTariff(this.state.item.screens_with_contents)<=0)
                    &&
                    <Button type="primary" onClick={() => this.props.onOk()}>
                        {lang.tr('Save')}
                    </Button>
                }
                
                {
                    this.state.current < steps.length - 1
                    &&
                    <Button type="primary" onClick={() => this.handleNextStep()}>{lang.tr('Next')}</Button>
                }
            </div>
        </Form>
    }
}

export default NewCampaign