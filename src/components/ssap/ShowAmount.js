import React, {PropTypes} from 'react'
import moment from 'moment'
import {config} from '../../utils'

class ShowAmount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: props.amount || 0,
        };
    }
    componentWillUpdate(nextProps, nextState){
        return true;
    }
    render() {
        return <span>{config.currencySymbol}{parseFloat(this.state.amount).toFixed(2)} {config.currencyName}&nbsp;</span>
    }
}
export default ShowAmount