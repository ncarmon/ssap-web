import React from 'react'
import {Table, Dropdown, Button, Menu, Icon, Modal,LocaleProvider,Badge,Spin} from 'antd'
import {TweenOneGroup} from 'rc-tween-one'
import styles from './profile.less'
import {request} from '../../utils'
const confirm = Modal.confirm
import {Row, Col,message} from 'antd';
import {  Card,Image } from 'semantic-ui-react';
import enUS from 'antd/lib/locale-provider/en_US';

class Profile extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      profile:{}
    }
    var me=this;
    request('user/get-profile',{},function(err,profile){
        if(profile){
            me.setState({profile:Array.isArray(profile)?profile[0]:profile});
        }else{
            message.error(err.message,3)
        }
        
    })
  }
  componentDidMount() {
  }
  componentWillUnmount() {
  }

  onEnd = (e) => {
    e.target.style.height = 'auto'
  }
  render () {
    const {
      loading,
      dataSource,
      pagination,
      onDeleteItem,
      onEditItem
    } = this.props   
    return <div className="profile-page">
     <Card>
        <Image src='/assets/user.png' />
        <Card.Content>
            <Card.Header>
            {this.state.profile.first_name} {this.state.profile.last_name}
            </Card.Header>
        <Card.Meta>
            <span className='date'>
                    
            </span>
        </Card.Meta>
        <Card.Description>
        {!this.state.profile.email?<Spin />:''}
        <p><a href={"mailto:"+this.state.profile.email}>{this.state.profile.email}</a></p>
        <p><a href={"tel:"+this.state.profile.username}>{this.state.profile.username}</a></p>
        </Card.Description>
        </Card.Content>
        {/* <Card.Content  textAlign='center'>
            <Button>Edit Profile</Button>
        </Card.Content> */}
    </Card>
</div>
  }
}

export default Profile