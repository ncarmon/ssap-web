import React from 'react'
import jax from 'react-jax'

export default class API extends React.Component {
    constructor(props) {
      super(props);      
    }  
    sendRequest(method,data,cb) {
        fetch('https://mywebsite.com/endpoint/', {
            method: method,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        .then((response) => response.json())
        .then((responseJson) => {
          return cb(null,responseJson);
        })
        .catch((error) => {
           return cb(error);
        });
    }
    post(data,cb){
        this.sendRequest('POST',data,cb)
    }
    get(cb){
        this.sendRequest('GET',data,cb)
    }
  }