import React, {PropTypes} from 'react'
import { Button, Row, Col, Form, Input,Select,message} from 'antd'
import { CountryList } from './country-list'
import {Link} from 'dva/router'
import { config,util,request,auth,lang } from '../../utils'
import styles from './login.less'
const Option=Select.Option;
const FormItem = Form.Item;

// let countryList=[];
// util.getAllCountryList().then(function(){
//   countryList=data.responseData;
// });  
const login = ({
  loginButtonLoading,
  changeSignUp,
  countryList,
  showForgot,
  showSignup,
  onOk,
  form: {
    getFieldDecorator,
    validateFieldsAndScroll
  }
}) => {
  function handleShowSignup(){
    showSignup();
  }
  function handleShowForgot(){
    showForgot();
  }
  
  function handleOk () {
    validateFieldsAndScroll((errors, formData) => {
      if (errors) {
        return
      }
      onOk({
        username: '+'+formData.country+"-"+formData.mobile,
        password: formData.password
      })
      // auth.login({
      //     username: formData.country+"-"+formData.mobile,
      //     password: formData.password
      //   },function(err,result){
      //     loginButtonLoading=false;
      //     if(err){
      //       return message.error(err.message,5);
      //     }
      //     onOk();
      //   })
    })
  }
  return (
    <div className={styles.form}>
      <div className={styles.logo}>
        {/* <img src={config.logoSrc} /> */}
        <span>{lang.tr("OSA Login")}</span>
      </div>
      <form>
      <FormItem hasFeedback>
          {getFieldDecorator('country', {
            rules: [
              {
                required: true,
                message: lang.tr("Please select country")
              }
            ]
          })(<Select notFoundContent={lang.tr("Not Found")} showSearch optionFilterProp={'desc'} placeholder={lang.tr("Please select country")}>
          {countryList.map((row, key) =>
              <Select.Option key={key} desc={row.country_name.toLowerCase()+' '+row.country_name+' +'+row.calling_code} value={row.calling_code+''}>{row.country_name} (+{row.calling_code})</Select.Option>
          )}
          </Select>)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: lang.tr("Please enter mobile number")
              }
            ]
          })(<Input type="number" onPressEnter={handleOk} placeholder={lang.tr("Mobile No")} />)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: lang.tr("Please enter your password")
              }
            ]
          })(<Input type='password' onPressEnter={handleOk} placeholder={lang.tr("Password")} />)}
        </FormItem>
        <Row>
          <Col>
            <Row>
              <Col md={13} offset={0}>
                    <Button  type='default'   onClick={handleShowForgot}>
                    {lang.tr("Forgot Password")}
                    </Button>
                </Col>
              <Col md={9} offset={2}>
                      <Button type='primary'   onClick={handleOk} loading={loginButtonLoading}>
                      {lang.tr("Login")}
                      </Button>
                    </Col>
          </Row>
          </Col>
          <Col>
                <Button  type='default'   onClick={handleShowSignup}>
                {lang.tr("Create New Account")}
                </Button>
              {/* </Link> */}
            </Col>
        </Row>
      </form>
    </div>
  )
}

login.propTypes = {
  changeSignUp:PropTypes.func,
  form: PropTypes.object,
  loginButtonLoading: PropTypes.bool,
  onOk: PropTypes.func
}

export default Form.create()(login)
