import React, {PropTypes} from 'react'
import {
    Search,
    Button,
    Modal,
    LocaleProvider,
    Input,
    Table,
    Icon,
    Tabs,
    Tag,
    List,
    Tooltip
} from 'antd'
import {request,lang,config} from '../../utils'
import {Row, Col, message} from 'antd';
import {Card, Image, Dimmer, Loader, Segment,Button as Btn} from 'semantic-ui-react';
import enUS from 'antd/lib/locale-provider/en_US';
import GoogleMap from 'google-map-react';
import PickerStyle from './ScreenPicker.less';
import moment from 'moment';
import  ShowDate from './ShowDate';
import controllable from 'react-controllables';
import ShowAmount from './ShowAmount';
class Screen extends React.Component {
    constructor(props) {
        super(props);
    }
    addToKart = () => {
        this.props.screen.added = true;
        this.setState({...this.state});
        this.props.onChange(this.props.screen);
    }
    removeFromKart = () => {
        this.props.screen.added = false;
        this.setState({...this.state});
        this.props.onChange(this.props.screen);
    }
    countPules=(contentList)=>{
        let total=0;
        contentList.map(function(item){
            total+=item.pulse;
            return item;
        });
        return total;
    }
    render() {
        const {
            screen,
            closeScreen
        } = this.props;
        return <Card key={'card' + screen.screen_id} style={this.props.style}>
                <Card.Content>
                    <Image floated='right' size='mini' src={screen.screen_photo}/>
                    <Card.Header>
                        <Icon type='desktop' /> {screen.screen_name}({screen.resolution})
                        {closeScreen ? <Icon type='close-circle-o' onClick={() => {
                            closeScreen(screen)
                        }} style={{
                            position: 'absolute',
                            right: '2px',
                            color: '#999',
                            cursor: 'pointer',
                            padding: '3px',
                            top: '2px'
                        }}/>:<span></span>
                        }
                    </Card.Header>
                    <Card.Meta>
                        <Icon type="environment" /> {screen.address}
                    </Card.Meta>
                    <Card.Description>
                        <Tabs size="small">
                            <Tabs.TabPane tab={<span><Icon type="desktop" /></span>} key="1" style={{textAlign:'center'}}>
                                <div>
                                    <span>
                                        <Tooltip title={<div><ShowAmount amount={screen.total_rate}/> X {this.countPules(this.props.content)} {lang.tr('pulse')}</div>}>
                                            <Icon type="pay-circle-o" /> <ShowAmount amount={screen.total_rate*this.countPules(this.props.content)}/>
                                        </Tooltip>
                                    </span>
                                    <span>
                                        <Icon type='eye' /> {screen.viewer_count}
                                    </span>
                                    <span>
                                        <Icon type='desktop' /> {screen.size}"
                                    </span>
                                </div>
                                <div>
                                    {(screen.tags || []).map((tag, key) =>
                                        <Tag key={key} closable={false} color="blue">{tag}</Tag>
                                    )}  
                                </div>
                            </Tabs.TabPane>
                            <Tabs.TabPane tab={<span><Icon type="clock-circle" /></span>} key="2">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>{lang.tr('Date')}</th>
                                            <th>{lang.tr('Availablity')}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {screen.availability.map((row, key) =>
                                        <tr key={key}>
                                            <td>
                                                <ShowDate date={row.start_date}/> to
                                                <ShowDate date={row.end_date}/></td>
                                            <td style={{textAlign:'right'}}>{row.available_pulses}</td>
                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </Tabs.TabPane>
                            {/* <Tabs.TabPane tab={<span><Icon type="calculator" /></span>} key="3">
                                <Table columns={[
                                    {
                                        title: 'Date',
                                        dataIndex: 'date',
                                        fixed: 'left',
                                        render: date => <ShowDate date={date}/>,
                                    },
                                    {
                                        title: 'Rate',
                                        fixed: 'right',
                                        dataIndex: 'rate'
                                    },
                                ]} dataSource={screen.tariffs} size="small" />
                            </Tabs.TabPane> */}
                        </Tabs>
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <div className='ui two buttons'>
                        {
                            !screen.added ?
                                <Button disabled={screen.min_availability<this.countPules(this.props.content)} size={'small'} color='green' onClick={() => {
                                    this.addToKart()
                                }}>{lang.tr('Add')}</Button> :
                                <Button size={'small'} color='red' onClick={() => {
                                    this.removeFromKart()
                                }}>{lang.tr('Remove')}</Button>
                        }
                    </div>
                </Card.Content>
            </Card>
    }
}
class MapPointer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {
            screen
        } = this.props;
        return <div style={{minWidth: '30px'}}>
            <Image width="30" src={'/assets/screen-icon.png'}/>
            {screen.active ? <Screen {...this.props} key={'card' + screen.screen_id} style={{minWidth:'300px',zIndex: 99999,marginTop: '-100px',marginLeft: '35px'}}/> : <div/>}
        </div>
    }
}

MapPointer.propTypes = {
    screen: PropTypes.object
};

@controllable(['center', 'zoom'])
class ScreenPicker extends React.Component {
    static propTypes = {
        center: PropTypes.array, // @controllable
        zoom: PropTypes.number, // @controllable
        onCenterChange: PropTypes.func, // @controllable generated fn
        onZoomChange: PropTypes.func, // @controllable generated fn
    };
    static defaultProps = {
        center: [59.838043, 30.337157],
        zoom: 9,
    };
    constructor(props) {
        super(props);
        this.state = {
            screens: props.value || [],
        };
        this._onSelectionChange();
        this.getNearByScreens();
    }
    countPules=(contentList)=>{
        let total=0;
        contentList.map(function(item){
            total+=item.pulse;
            return item;
        });
        return total;
    }
    componentDidMount() {
    }

    componentWillUnmount() {
    }

    getCurrentPosition() {
        return new Promise((resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject));
    }

    getNearByScreens() {
        let me = this;
        me.getCurrentPosition().then(function (data) {
            me.getNearByScreensByLatLng(data.coords.latitude,data.coords.longitude)
            me.setState({...me.state, map: {lat: data.coords.latitude, lng: data.coords.longitude, zoom: 10}});
        }, function () {

        });

    }

    getNearByScreensByLatLng(lat, lng, redius, hideLoading) {
        redius = redius || 10000;
        let me = this;
        me.updateSearchStatus(!hideLoading);
        request('screens/nearby-screens', {
            data: {
                "lat": lat,
                "long": lng,
                start_date:me.props.startDate,
                end_date:me.props.endDate,
                "radius_meters": redius
            }
        }, function (err, data) {
            me.updateSearchStatus(false);
            if (data) {
                me.mergeScreenResult(data.list);
            }
        });
    }
    getContens=function(screen,row){
        screen.availability=screen.availability || [];
        for(var i=0;i<screen.availability.length;i++){
            if(screen.availability[i].start_date.format("DD/MM/YYYY")==row.week_start.format("DD/MM/YYYY") && screen.availability[i].end_date.format("DD/MM/YYYY")==row.week_end.format("DD/MM/YYYY")){
                return screen.availability[i].contents || [];
            }
        }
        if(screen.weeks){
            for(var i=0;i<screen.weeks.length;i++){
                if(moment(screen.weeks[i].start_date).format("DD/MM/YYYY")==row.week_start.format("DD/MM/YYYY") && moment(screen.weeks[i].end_date).format("DD/MM/YYYY")==row.week_end.format("DD/MM/YYYY")){
                    return screen.weeks[i].contents || [];
                }
            }
        }
        
        return [];
    }
    calculateAvailablability=function(screen){
        let me=this;
        screen.total_availability=0;
        //if(!screen.availability){
            screen.min_availability=config.max_pulses;
            screen.availability=screen.availablePulse.map(function (row) {
                screen.total_availability+=row.pulse;
                screen.min_availability=Number(Math.min(screen.min_availability,row.pulse));
                row.week_start=moment(row.week_start);
                row.week_end=moment(row.week_end);
                return {
                    contents:me.getContens(screen,row),
                    available_pulses:row.pulse,
                    start_date:row.week_start,
                    end_date:row.week_end,
                }
            })
        //}
        return screen;
    }
    mergeScreenResult = (list) => {
        let me = this;
        let oldScreenCnt=me.state.screens.length;
        
        (list || []).forEach((screen) => {
            let found = false;
            screen.tariff=screen.tariff || [];
            screen.total_rate=0;
            screen.tariffs=screen.tariff.map(function (row) {
                screen.total_rate+=Number(row.tariff);
                return {
                    rate:row.tariff,
                    date:moment(row.date)
                }
            });
            for (let i = 0; i < me.state.screens.length; i++) {
                if (me.state.screens[i].screen_id == screen.screen_id) {
                    Object.assign(me.state.screens[i], screen);
                    found = true;
                    break;
                }
                me.state.screens[i]=this.calculateAvailablability(me.state.screens[i]);
            }
            if (!found) {
                screen=this.calculateAvailablability(screen);
                me.state.screens.push({...screen});
            }
        });
        if(me.state.screens.length && oldScreenCnt==0){
            this.props.onCenterChange([me.state.screens[0].lat, me.state.screens[0].long]);
        }
        me.setState({...me.state})
    };
    timeout;
    handleSearchAutoScreen(txt){
        clearTimeout(this.timeout);
        this.timeout=setTimeout(()=>{
            this.handleSearchScreen(txt);
        },500);
    };
    handleSearchScreen(keyword) {
        let me = this;
        me.state.searchKeyword=keyword;
        me.updateSearchStatus(true);
        request('screens/screens-details', {
            data: {
                screen_tags:keyword,
                start:0,
                limit:25,
                start_date:me.props.startDate,
                end_date:me.props.endDate,
            }
        }, function (err, data) {
            me.updateSearchStatus(false);
            if (data) {
                me.state.screens=[];
                me.mergeScreenResult(data.list);
            }
        });
    }

    updateSearchStatus = (flag) => {
        this.setState({...this.state, searching: !!flag})
    };
    _onZoomChange=()=>{
        this.state.screens.forEach((screen)=>{
            screen.active=false;
        })
        this.setState({...this.state});
    }
    _onBoundsChange = (center, zoom) => {
        if(!this.state.searchKeyword){
            this.getNearByScreensByLatLng(center[0], center[1], (22000 - (zoom * 1000)), true);
        }

    };
    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.mergeScreenResult(value);
        }
    }
    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(changedValue);
        }
    }
    _onSelectionChange = (lastChanged) => {
        var results=this.state.screens.filter((item)=>{
            return item.added
        });
        if(results.length==0){
            results=undefined;
        }
        this.triggerChange(results);
    }
    _onScreenMarkerClick = (key, childProps) => {
        this.markScreenActive(childProps.screen);
    };
    markScreenActive = (screen) => {
        this.state.screens.forEach(row => {
            row.active = screen.screen_id == row.screen_id;
        });
        this.props.onCenterChange([screen.lat, screen.long]);
        this.setState({...this.state});
    }
    onEnd = (e) => {
        e.target.style.height = 'auto'
    }

    render() {
        const {
            loading,
            dataSource,
            pagination,
            onDeleteItem,
            onEditItem
        } = this.props;
        const SearchPagination = {
            pageSize: 10,
            current: 1,
            total: this.state.screens.length,
            onChange: (() => {
            }),
        };

        return <Row style={{marginBottom:0}}>
            {
                this.props.disabled
                &&
                <div style={{width: '100%',
                    'height': '100%',
                    'position': 'absolute',
                    'background': '#00000078',
                    'zIndex': 11}}/>
            }
            <Col span={18}>
                <div style={{position: 'relative', width: '100%', height: '400px'}}>
                    <GoogleMap
                        
                        center={this.props.center}
                        zoom={this.props.zoom}
                        onBoundsChange={this._onBoundsChange}
                        onChildClick={this._onScreenMarkerClick}
                        onZoomAnimationEnd={this._onZoomChange}
                    >
                        {this.state.screens.map((row, key) =>
                            <MapPointer
                                key={row.screen_id}
                                screen={row}
                                content={this.props.content}
                                closeScreen={(screen)=>{
                                    screen.active=false;
                                    this.setState({...this.state});
                                }}
                                onChange={(screen) => {
                                    this._onSelectionChange(screen);
                                }}
                                lat={row.lat}
                                lng={row.long}
                            />
                        )}
                    </GoogleMap>
                </div>
            </Col>
            <Col span={6}>
                <Row>
                    <Col span={20} style={{padding: '0px 10px'}}>
                        <Input.Search
                            placeholder={lang.tr("Search with address, tags")}
                            onChange={(e) => {this.handleSearchAutoScreen(e.target.value)}}
                            onSearch={value => this.handleSearchScreen(value)}
                        />
                    </Col>
                </Row>
                {
                    this.state.screens.length&&
                    <Row>
                        <Col span={20} style={{padding: '0px 10px'}}>
                        <Btn compact as="a" size="tiny" basic color='grey' fluid onClick={() => {
                           let countAdded=0;
                           this.state.screens.map((screen)=>{
                            if(screen.min_availability>=this.countPules(this.props.content)){
                                screen.added=true;
                                countAdded++;
                            }
                           });
                           this.setState({...this.state});
                           if(countAdded){
                            message.success(lang.tr('Added')+' '+countAdded+' '+lang.tr('screens'))
                           }else{
                            message.error(lang.tr('Screens not availables, try to change dates or pulses and try again'))
                           }
                           
                           this._onSelectionChange(); 
                        }}>{lang.tr('Add All')}</Btn>
                        </Col>
                    </Row>
                }
                <Row style={{marginBottom:0}}>
                    <Col style={{padding: '0px 10px',maxHeight: '360px',overflow: 'scroll',paddingTop: '12px',marginTop:'-9px'}}>
                        {this.state.searching ? (<Segment>
                                <Dimmer>
                                    <Loader size='mini'>{lang.tr('Loading')}</Loader>
                                </Dimmer>
                                <Image src='/assets/image-text.png'/>
                            </Segment>) :
                            (<List itemLayout="vertical" size="large"
                                   pagination={SearchPagination}
                                   dataSource={this.state.screens}
                                   renderItem={row => (
                                       <List.Item key={'result' + row.screen_id} onClick={()=>{this.markScreenActive(row)}}>
                                           <Screen
                                               content={this.props.content}
                                               screen={row}
                                               onChange={(screen) => {
                                                   this._onSelectionChange(screen);
                                               }}
                                               key={'card' + screen.screen_id}
                                               style={{minWidth:'100%',backgroundColor:row.active?'#fafafa':'#FFF'}}/>
                                       </List.Item>
                                   )}
                            />)
                        }
                    </Col>
                </Row>
            </Col>
        </Row>
    }
}

export default ScreenPicker