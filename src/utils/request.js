const Ajax = require('robe-ajax')
const config = require('./config')
import { message } from 'antd';
import moment from 'moment-timezone';
export default function request(service, options,cb,progress) {
  options=options || {};
  let publicServiceList=['user/login','user/signup','ref-data/country-codes','user/forgot-password']
  let cb1=function(){};
  cb=cb || cb1;
  let url=config.api_endpoint; 
  let headers={
    Authorization:config.api_basic_auth
  }
  let accessToken=localStorage.getItem('access_token')
  if(accessToken && publicServiceList.indexOf(service)<0){
    headers.Authorization='Bearer '+accessToken;
    
  }
  if(!options.upload){
    headers['Content-Type']='application/json';
  }
  let data=options.data;
  let reqData={
    "header":{
      "version": 1.2,
      "txName": service,
      "lang": "EN",
      tz:moment.tz.guess()
    },
    "reqData":data || {}
  }
  return Ajax.ajax({
    url: url+service,
    method: options.method || 'post',
    headers:headers,
    data: !options.upload?JSON.stringify(reqData):options.data,
    processData: false,
    cache:false,
    'contentType': !options.upload?'application/json':false,
    dataType: 'JSON'

  }).done((data) => {
    if(data && data.result && data.result.status){
      cb(null,data.responseData)
    }else if(data.result){
      var err=data.errors && data.errors.length?data.errors[0]:data.result
      cb(err);
      if(err && err.code=='ERR009'){
        window.dispatch({type: 'app/logout'});
      }
    }else{
      cb({message:'Unknown error'})
    }
  }).fail((data) => {
    cb({message:'Server is not reacable'})
  })
}
