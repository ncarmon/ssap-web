import React, {PropTypes} from 'react'
import { Button, Row, Col, Form, Input,Select,message} from 'antd'
import { CountryList } from './country-list'
import {Link} from 'dva/router'
import { config,util,request,auth } from '../../utils'
//import styles from './login.less'
const Option=Select.Option;
const FormItem = Form.Item;

// let countryList=[];
// util.getAllCountryList().then(function(){
//   countryList=data.responseData;
// });  
const Profile = ({
  profile,
  UpdateProfile,
  form: {
    getFieldDecorator,
    validateFieldsAndScroll
  }
}) => {
  function handleShowSignup(){
    showSignup();
  }
  function handleShowForgot(){
    showForgot();
  }
  
  function handleOk () {
    validateFieldsAndScroll((errors, formData) => {
      if (errors) {
        return
      }
      UpdateProfile(formData)
    })
  }
  return (
    <div>
      <div>
        {/* <img src={config.logoSrc} /> */}
        <span>Update Profile</span>
      </div>
      <form>
      <FormItem hasFeedback>
          {getFieldDecorator('country', {
            rules: [
              {
                required: true,
                message: 'Please select country'
              }
            ]
          })(<Select notFoundContent='Not Found' showSearch optionFilterProp={'desc'} placeholder="Please select a country">
          {countryList.map((row, i) =>
              <Option key={'login'+i} desc={row.country_name.toLowerCase()+' '+row.country_name+' +'+row.calling_code} value={row.calling_code+''}>{row.country_name} (+{row.calling_code})</Option>
          )}
          </Select>)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: 'Please enter mobile number'
              }
            ]
          })(<Input type="number" size='large' onPressEnter={handleOk} placeholder='Mobile No' />)}
        </FormItem>
        <FormItem hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Please enter your password'
              }
            ]
          })(<Input size='large' type='password' onPressEnter={handleOk} placeholder='Password' />)}
        </FormItem>
        <Row>
          <Col>
            <Row>
              <Col md={13} offset={0}>
                    <Button  type='default' size='large' onClick={handleShowForgot}>
                      Forgot Password
                    </Button>
                </Col>
              <Col md={9} offset={2}>
                      <Button type='primary' size='large' onClick={handleOk} loading={loginButtonLoading}>
                        Login
                      </Button>
                    </Col>
          </Row>
          </Col>
          <Col>
                <Button  type='default' size='large' onClick={handleShowSignup}>
                  Create New Account
                </Button>
              {/* </Link> */}
            </Col>
        </Row>
      </form>
    </div>
  )
}

Profile.propTypes = {
  UpdateProfile:PropTypes.func,
  form: PropTypes.object,
  profile: PropTypes.object,
}

export default Form.create()(Profile)
