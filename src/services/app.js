import { request } from '../utils'

export async function login (params) {
  return request('user/login', {
    method: 'post',
    data: params
  })
}
export async function getCountryList (params) {
  return request('ref-data/country-codes', {
    method: 'post',
    data: params
  })
}
export async function signup (params) {
  return request('user/signup', {
    method: 'post',
    data: params
  })
}

export async function logout (params) {
  return request('/logout', {
    method: 'post',
    data: params
  })
}

export async function forgotPassword (params) {
  return request('user/forgot-password', {
    method: 'post',
    data: params
  })
}

export async function userInfo (params) {
  return request('user/get-profile', {
    method: 'post',
    data: params
  })
}
