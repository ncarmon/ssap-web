import { request } from '../utils'

export async function query (params) {
  return request('campaign/list-campaign', {
    data: params
  })
}

export async function create (params) {
  return request('campaign/add-campaign', {
    method: 'post',
    data: params,
    upload:false
  })
}

export async function remove (params) {
  return request('campaign/remove', {
    data: params
  })
}

export async function update (params) {
  return request('campaign/add-campaign', {
    method: 'post',
    data: params,
    upload:false
  })
}
export async function get (params) {
  return request('campaign/details-campaign', {
    method: 'post',
    data: params
  })
}