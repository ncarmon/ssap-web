import {login, userInfo, logout,getCountryList,signup,forgotPassword} from '../services/app'
import {parse} from 'qs'
import {message} from 'antd'
import { window } from 'd3-selection';
import { lang } from '../utils';
//import signup from '../routes/ssap-pages/signup';

export default {
  namespace : 'app',
  state : {
    login: false,
    loading: true,
    lock:false,
    SignUp:false,
    user: {
      name: 'DB'
    },
    countryList:[],
    loginButtonLoading: false,
    menuPopoverVisible: false,
    siderFold: localStorage.getItem('berrdAdminSiderFold') === 'true',
    darkTheme: true,//localStorage.getItem('berrAdminDarkTheme') !== 'false',
    isNavbar: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(localStorage.getItem('navOpenKeys') || '[]') //The sidebar menu opens the keys
  },
  subscriptions : {
    setup({dispatch}) { 
      window.dispatch=dispatch;
      let dir=lang.getDir();
      document.body.dir=dir;
      dispatch({type: 'appInit'});
      window.onresize = function () {
        dispatch({type: 'changeNavbar'})
      }
    }
  },
  effects : {
    *signup({
      payload
    }, {call, put}) {
      yield put({type: 'showLoginButtonLoading'})
      const data = yield call(signup, parse(payload))
      if (data.result && data.result.status) {
        yield put({
          type: 'loginSuccess',
          payload: {
            token:data.responseData            
          }
        })
        yield put({type: 'afterLogin'})
      } else {
        if(data.errors && data.errors.length){
          message.error(data.errors[0].message,3);
        }
        yield put({type: 'loginFail'})
      }
    },
    *forgotPassword({
      payload
    }, {call, put}) {
      yield put({type: 'showLoginButtonLoading'})
      const data = yield call(forgotPassword, parse(payload))
      if (data.result && data.result.status) {
        message.success('Temporary password has sent to registered email & mobile',3);
        yield put({
          type: 'forgotSuccess',
          payload: {
            token:data.responseData            
          }
        })
      } else {
        if(data.errors && data.errors.length){
          message.error(data.errors[0].message,3);
        }
        yield put({type: 'forgotFail'})
      }
    },
    *afterLogin({payload},{call, put}){
      yield put({type: 'contents/query'});
      yield put({type: 'campaigns/query'});
    },
    *login({
      payload
    }, {call, put}) {
      yield put({type: 'showLoginButtonLoading'})
      const data = yield call(login, parse(payload))
      if (data.result && data.result.status) {
        
        
        yield put({
          type: 'loginSuccess',
          payload: {
            token:data.responseData            
          }
        })
        yield put({type: 'afterLogin'})
      } else {
        if(data.errors && data.errors.length){
          message.error(data.errors[0].message,3);
        }
        yield put({type: 'loginFail'})
      }
    },
    *queryUser({
      payload
    }, {call, put}) {
      yield put({type: 'showLoading'})
      const data = yield call(userInfo, parse(payload))
      if (data.success) {
        yield put({
          type: 'loginSuccess',
          payload: {
            user: {
              name: data.username
            }
          }
        })
      }

      yield put({type: 'hideLoading'})
    },
    *appInit({
      payload
    }, {call, put}) {
      var tokenData=localStorage.getItem('token');
      try{
        tokenData=JSON.parse(tokenData)
      }catch(e){
        tokenData=null;
      }
      if(tokenData){
        yield put({
          type: 'loginSuccess',
          payload: {
            token:tokenData
          }
        })
      }
      const data = yield call(getCountryList, parse(payload))
      if (data.result && data.result.status) {
        yield put({
          type: 'countryLoaded',
          payload: {
            countryList:data.responseData
          }
        })
      }
      yield put({type: 'hideLoading'})
    },
    *logout({
      payload
    }, {call, put}) {
      //const data = yield call(logout, parse(payload))
      yield put({type: 'logoutSuccess'})
    },
    *showLogin({
      payload
    }, {call, put}) {
      yield put({type: 'showLoginForm'})
    },
    *showForgotPassword({
      payload
    }, {call, put}) {
      yield put({type: 'showForgotPasswordForm'})
    },
    *showSignup({
      payload
    }, {call, put}) {
      yield put({type: 'showSignupForm'})
    },
    *loadCountry({
      payload
    }, {call, put}) {
      const data = yield call(getCountryList, parse(payload))
      if (data.result && data.result.status) {
        yield put({
          type: 'countryLoaded',
          payload: {
            countryList:data.responseData
          }
        })
      }
    },
    *switchSider({
      payload
    }, {put}) {
      yield put({type: 'handleSwitchSider'})
    },
    *changeTheme({
      payload
    }, {put}) {
      yield put({type: 'handleChangeTheme'})
    },
    *changeLock({
      payload
    }, {put}) {
      yield put({type: 'handleLock'})
    },
    *changeSignUp({
      payload
    }, {put}) {
      yield put({type: 'handleSignUp'})
    },
    *changeNavbar({
      payload
    }, {put}) {
      if (document.body.clientWidth < 769) {
        yield put({type: 'showNavbar'})
      } else {
        yield put({type: 'hideNavbar'})
      }
    },
    *switchMenuPopver({
      payload
    }, {put}) {
      yield put({type: 'handleSwitchMenuPopver'})
    }
  },
  reducers : {
    
    countryLoaded(state, action) {
      return {
        ...state,
        ...action.payload,
        countryList: action.payload.countryList,
      }
    },
    forgotFail(state, action){
      return {
        ...state,
        loginButtonLoading: false
      }
    },
    forgotSuccess(state, action){
      return {
        ...state,
        SignUp:false,
        showForgotPassword:false,
        loginButtonLoading: false
      }
    },
    loginSuccess(state, action) {
      localStorage.setItem('token',JSON.stringify(action.payload.token));
      localStorage.setItem('access_token',action.payload.token.access_token);
      return {
        ...state,
        ...action.payload,
        login: true,
        SignUp:false,
        loginButtonLoading: false
      }
    },
    showForgotPasswordForm(state) {
      return {
        ...state,
        showForgotPassword: true
      }
    },
    showSignupForm(state) {
      return {
        ...state,
        showForgotPassword: false,
        SignUp: true
      }
    },
    showLoginForm(state) {
      return {
        ...state,
        showForgotPassword:false,
        SignUp: false
      }
    },
    logoutSuccess(state) {
      localStorage.setItem('token',undefined);
      localStorage.setItem('access_token',undefined);
      return {
        ...state,
        login: false
      }
    },
    loginFail(state) {
      return {
        ...state,
        login: false,
        loginButtonLoading: false
      }
    },
    showLoginButtonLoading(state) {
      return {
        ...state,
        loginButtonLoading: true
      }
    },
    showLoading(state) {
      return {
        ...state,
        loading: true
      }
    },
    hideLoading(state) {
      return {
        ...state,
        loading: false
      }
    },
    handleSwitchSider(state) {
      localStorage.setItem('antdAdminSiderFold', !state.siderFold)
      return {
        ...state,
        siderFold: !state.siderFold
      }
    },
    handleChangeTheme(state) {
      localStorage.setItem('antdAdminDarkTheme', !state.darkTheme)
      return {
        ...state,
        darkTheme: !state.darkTheme
      }
    },
    handleLock(state) {
      return {
        ...state,
        lock: !state.lock
      }
    },
    handleSignUp(state) {
      return {
        ...state,
        SignUp: !state.SignUp
      }
    },
    showNavbar(state) {
      return {
        ...state,
        isNavbar: true
      }
    },
    hideNavbar(state) {
      return {
        ...state,
        isNavbar: false
      }
    },
    handleSwitchMenuPopver(state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible
      }
    },
    handleNavOpenKeys(state, action) {
      return {
        ...state,
        ...action.payload
      }
    }
  }
}
