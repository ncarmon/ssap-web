import { request } from '../utils'

export async function query (params) {
  return request('content/list', {
    data: params
  })
}

export async function create (params) {
  return request('content/add-content', {
    method: 'post',
    data: params,
    upload:true
  })
}

export async function remove (params) {
  return request('content/remove', {
    data: params
  })
}

export async function update (params) {
  return request('/api/users', {
    method: 'put',
    data: params
  })
}
