import React, {PropTypes} from 'react'
import { Upload, Button, Icon, message,Spin } from 'antd';
import { request,lang } from '../../utils'

class UploadFile extends React.Component {
  constructor(props) {
    super(props);

    const value = this.props.value || {};
    this.state = {
      fileList: [],
      uploading: false,
    }
  }
  componentWillReceiveProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value;
      this.setState(value);
    }
  }
  handleNumberChange = (e) => {
    const number = parseInt(e.target.value || 0, 10);
    if (isNaN(number)) {
      return;
    }
    if (!('value' in this.props)) {
      this.setState({ number });
    }
    this.triggerChange({ number });
  }
  handleCurrencyChange = (currency) => {
    if (!('value' in this.props)) {
      this.setState({ currency });
    }
    this.triggerChange({ currency });
  }
  handleUpload = (info) => {
    let me=this;
    setTimeout(function(){
    const { fileList } = me.state;
    const formData = new FormData();
    formData.append('file', fileList[0]);
    me.setState({
      uploading: true,
    });

    // You can use any AJAX library you like
    
    request('content/add-content',{data:formData,upload:true},function(err,data){
      if(!err){
        me.setState({
          fileList: [],
          uploading: false,
        });
        if (!('value' in me.props)) {
          me.setState({ file:data });
        }
        me.triggerChange({ file:data });
        if(me.props.onComplete){
          me.props.onComplete();
        }
      }else{
        me.setState({
          uploading: false,
        });
        message.error(err.message);
      }
    },function(progress){
      console.log(progress)
    })
    },100)
  }
  triggerChange = (changedValue) => {
    // Should provide an event to pass value to Form.
    const onChange = this.props.onChange;
    if (onChange) {
      onChange(Object.assign({}, this.state, changedValue));
    }
  }

render() {
    const { uploading } = this.state;
    const props = {
      multiple:false,
      onRemove: (file) => {
        this.setState(({ fileList }) => {
          const index = fileList.indexOf(file);
          const newFileList = fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState(({ fileList }) => ({
          fileList: [file],
        }));
        return false;
      },
      onProgress:(progress)=>{
        console.log(progress);
      },
      fileList: this.state.fileList,
    };

    return (
      <div>
        
        <Upload.Dragger accept={'video/*,image/*'} {...props} onChange={this.handleUpload}>
        <p className="ant-upload-drag-icon">
        {
          uploading
          &&
          <div><Spin size="large" /> {lang.tr('Uploading...')}</div>
        }
        {
          !uploading
          &&
          <Icon type="inbox" />
        }
        </p>
        <p className="ant-upload-text">{lang.tr('Click or drag file to this area to upload')}</p>
        <p className="ant-upload-hint">{lang.tr('Only Support Video | Image files')}</p>
        </Upload.Dragger>
      </div>
    );
  }
}



export default UploadFile