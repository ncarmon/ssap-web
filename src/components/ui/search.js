import React from 'react'
import ReactDOM from 'react-dom'
import styles from './search.less'
import { Input, Select, Button, Icon } from 'antd'
import {lang} from '../../utils' 
class Search extends React.Component {
  state = {
    clearVisible: false,
    selectValue: (this.props.select && this.props.selectProps) ? this.props.selectProps.defaultValue : ''
  }
  handleSearch = () => {
    const data = {
      keyword: ReactDOM.findDOMNode(this.refs.searchInput).value
    }
    this.setState({
      ...this.state,
      keyword:data.keyword,
      clearVisible: !!data.keyword
    })
    if (this.props.select) {
      data.field = this.state.selectValue
    }
    this.props.onSearch && this.props.onSearch(data)
  }
  handleInputChange = e => {
    let value=ReactDOM.findDOMNode(this.refs.searchInput).value;
    this.setState({
      ...this.state,
      selectValueD: value,
      clearVisible: !!value
    })
  }
  handeleSelectChange = value => {
    this.setState({
      ...this.state,
      selectValue: value,
      selectValueD: value
    })
  }
  handleClearInput = () => {
    ReactDOM.findDOMNode(this.refs.searchInput).value = ''
    this.setState({
      ...this.state,
      selectValueD:false,
      clearVisible: false
    })
    this.handleSearch()
  }
  render () {
    const {size, select, selectOptions, selectProps, style, keyword} = this.props
    var {clearVisible,selectValueD} = this.state
    clearVisible=!!keyword;
    return (
      <Input.Group size={size} className={styles.search} style={style}>
        {select && <Select ref='searchSelect' onChange={this.handeleSelectChange} size={size} {...selectProps}>
          {selectOptions && selectOptions.map((item, key) => <Select.Option value={item.value} key={key}>{item.name || item.value}</Select.Option>)}
        </Select>}
        <Input ref='searchInput' size={size} onChange={this.handleInputChange} onPressEnter={this.handleSearch} defaultValue={keyword}/>
        <Button size={size} type='primary' onClick={this.handleSearch}>{lang.tr('Search')}</Button>
        {(selectValueD) && <Icon type='cross' style={{padding:0,right:'90px'}} onClick={this.handleClearInput} />}
      </Input.Group>
    )
  }
}

export default Search
