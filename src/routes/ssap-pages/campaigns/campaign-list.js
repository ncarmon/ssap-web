import React, {PropTypes} from 'react'
import {routerRedux} from 'dva/router'
import {connect} from 'dva'
import CampaignList from '../../../components/ssap/campaigns/list'
import CampaignSearch from '../../../components/ssap/campaigns/search'
import CampaignModal from '../../../components/ssap/campaigns/modal'
import {Row, Col} from 'antd';

function Campaigns({location, dispatch, campaigns}) {
  const {
    loading,
    list,
    pagination,
    currentItem,
    modalVisible,
    modalType
  } = campaigns
  const {field, keyword} = location.query

  const userModalProps = {
    item: currentItem||{},
    type: modalType,
    visible: modalVisible,
    onOk(data) {
      dispatch({type: `campaigns/${modalType}`, payload: data})
    },
    onCancel() {
      dispatch({type: 'campaigns/hideModal'})
    }
  }

  const userListProps = {
    dataSource: list,
    loading,
    pagination: pagination,
    onPageChange(page,filter,sort) {
      const {query, pathname} = location
      dispatch(routerRedux.push({
        pathname: pathname,
        query: {
          ...query,
          page: page.current,
          sort_by:sort.field,
          filter:JSON.stringify(filter),
          sort_order:sort.order,
          pageSize: page.pageSize
        }
      }))
    },
    onDeleteItem(id) {
      dispatch({type: 'campaigns/delete', payload: id})
    },
    onEditItem(item) {
      dispatch({
        type: 'campaigns/get',
        payload: {
          modalType: 'update',
          currentItem: item
        }
      })
    }
  }
  const userSearchProps = {
    field,
    keyword,
    onSearch(fieldsValue) {
      fieldsValue.keyword.length
        ? dispatch(routerRedux.push({
          pathname: '/campaigns',
          query: {
            field: fieldsValue.field,
            keyword: fieldsValue.keyword
          }
        }))
        : dispatch(routerRedux.push({pathname: '/campaigns'}))
    },
    onAdd() {
      dispatch({
        type: 'campaigns/showModal',
        payload: {
          modalType: 'create'
        }
      })
    }
  }

  const CampaignModalGen = () => <CampaignModal {...userModalProps}/>
  

  return (
    <div className='content-inner'>
      <Row>
        <Col xs={24} md={24} lg={24}>
          <CampaignSearch {...userSearchProps}/>
          <CampaignList {...userListProps}/>
          <CampaignModalGen/>
        </Col>

      </Row>

    </div>
  )
}

Campaigns.propTypes = {
  campaigns: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func
}

function mapStateToProps({campaigns}) {
  return {campaigns}
}

export default connect(mapStateToProps)(Campaigns)
