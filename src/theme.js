module.exports = () => {
  return {
    '@border-radius-base': '1px',
    '@border-radius-sm': '0px',
    '@shadow-color': 'rgba(0,0,0,0.05)',
    '@shadow-1-down': '4px 4px 40px @shadow-color',
    '@border-color-split': '#f4f4f4',
    '@border-color-base': '#e5e5e5',
    '@menu-dark-bg': '#3e3e3e',
    '@text-color': '#666',
    '@font-family': "'Open Sans', sans-serif"
  }
}
