import React, {PropTypes} from 'react'
import {routerRedux} from 'dva/router'
import {connect} from 'dva'


import {Row, Col, Card, Icon} from 'antd';

function PaymentPage({location, dispatch, users}) {
  const {
    loading,
    list,
    pagination,
    currentItem,
    modalVisible,
    modalType
  } = users
  const {field, keyword} = location.query
  setTimeout(function(){
    window.location.href='#/campaigns'
  },3000);
  return (
    <div className='content-inner'>
      <Row align="middle">
        {
          location.pathname.indexOf('success')>=0
          &&
          <Col span={24}>
            <Card style={{paddingTop:'10%',paddingBottom:'10%',textAlign:'center'}} cover={<Icon type="check-circle" style={{marginTop:'20px',fontSize: '80px',color:'green'}} />}>
              <p>Payment completed successfully.</p>
            </Card>
          </Col>
        }
        {
          location.pathname.indexOf('success')<0
          &&
          <Col span={24}>
            <Card style={{paddingTop:'10%',paddingBottom:'10%',textAlign:'center'}} cover={<Icon type="exclamation-circle" style={{marginTop:'20px',fontSize: '80px',color:'red'}} />}>
              <p>Payment failed.</p>
            </Card>
          </Col>
        }
        
        
      </Row>

    </div>
  )
}

PaymentPage.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func
}

function mapStateToProps({users}) {
  return {users}
}

export default connect(mapStateToProps)(PaymentPage)
