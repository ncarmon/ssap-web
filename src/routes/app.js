import React, {PropTypes} from 'react'
import {connect} from 'dva'
import Login from './ssap-pages/login'
import Signup from './ssap-pages/signup'
import Forgot from './ssap-pages/forgot'
import Header from '../components/layout/header'
import LockPage from '../routes/pages/lockscreen'
import {Redirect} from 'dva/router'
import Bread from '../components/layout/bread'
import Footer from '../components/layout/footer'
import Sider from '../components/layout/sider'
import styles from '../components/layout/main.less'
import {Spin, LocaleProvider, Switch} from 'antd'
import {classnames, config,util} from '../utils'
import '../components/layout/common.less'
import enUS from 'antd/lib/locale-provider/en_US';

function onAppInit(dispatch) {
  return (nextState, replace, callback) => {
    dispatch({type: 'app/loadCountry'});
  };
}
function App({children, location, dispatch, app}) {
  //dispatch({type: 'app/loadCountry'});
  window.dispatch=dispatch;
  const {
    countryList,
    login,
    loading,
    showForgotPassword,
    loginButtonLoading,
    user,
    siderFold,
    darkTheme,
    isNavbar,
    menuPopoverVisible,
    navOpenKeys,
    lock,
    SignUp
  } = app
  //let login=!!(localStorage.getItem('access_token'))
  
  const loginProps = {
    loading,
    loginButtonLoading,    
    countryList,
    showLogin(){
      dispatch({type: 'app/showLogin'})
    },
    showForgot(){
      dispatch({type: 'app/showForgotPassword'}) 
    },
    forgotPassword(data){
      dispatch({type: 'app/forgotPassword',payload: data}) 
    },
    showSignup(){
      dispatch({type: 'app/showSignup'}) 
    },
    onOk(data) {
      dispatch({type: 'app/login', payload: data})
    },
    onOkSignup(data) {
      dispatch({type: 'app/signup', payload: data})
    },
    changeSignUp(data) {
      dispatch({type: 'app/changeSignUp', payload: data})
    }
  }
  const headerProps = {
    user,
    siderFold,
    location,
    isNavbar,
    menuPopoverVisible,
    navOpenKeys,
    switchMenuPopover() {
      dispatch({type: 'app/switchMenuPopver'})
    },
    logout() {
      dispatch({type: 'app/logout'})
    },
    switchSider() {
      dispatch({type: 'app/switchSider'})
    },
    changeLock() {
      dispatch({type: 'app/changeLock'})
    },
    changeSignUp() {
      dispatch({type: 'app/changeSignUp'})
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys))
      dispatch({
        type: 'app/handleNavOpenKeys',
        payload: {
          navOpenKeys: openKeys
        }
      })
    }
  }

  const siderProps = {
    siderFold,
    darkTheme,
    location,
    navOpenKeys,
    changeTheme() {
      dispatch({type: 'app/changeTheme'})
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys))
      dispatch({
        type: 'app/handleNavOpenKeys',
        payload: {
          navOpenKeys: openKeys
        }
      })
    },
    changeLock() {
      dispatch({type: 'app/changeLock'})
    },
    changeSignUp() {
      dispatch({type: 'app/changeSignUp'})
    }
  }
  var publicRoutes=['/signup','/login'];
  if(showForgotPassword){
    return (
      <div>

          <div className={styles.spin}>
            <Forgot {...loginProps}/>
          </div>

        </div>
    )
  }
  if (SignUp) {
    return (
      <div>

          <div className={styles.spin}>
            <Signup {...loginProps}/>
          </div>

        </div>
    )
  } else if (lock) {
    return (
      <div>
        <LockPage/>
      </div>
    )

  } else if (config.needLogin) {
    if (!login) {
      return (
        <div>

          <div className={styles.spin}>
            <Login {...loginProps}/>
          </div>

        </div>
      )
    }
  } 
  
  if ((login || !config.needLogin)) {
    return (
      <div
        className={classnames(styles.layout, {
        [styles.fold]: isNavbar
          ? false
          : siderFold
      }, {
        [styles.withnavbar]: isNavbar
      })}>
        {!isNavbar
          ? <aside
              className={classnames(styles.sider, {
              [styles.light]: !darkTheme
            })}>
              <Sider {...siderProps}/>
            </aside>
          : ''}

        <div className={styles.main}>
          <div className={styles.spin}>
            <Spin tip='Loading...' spinning={loading} size='large'>
              <Header {...headerProps}/>
              <div className={styles.container}>
                <div className={styles.content}>
                  {children}
                </div>
              </div>
              <Footer/>
            </Spin>
          </div>
        </div>

      </div>
    )
  }

  if(publicRoutes.indexOf(location.pathname)>=0){
    if(location.pathname=='/login'){
 return (
        <div className={styles.outmain}>
                <div className={styles.spin}>
                <Login {...loginProps}/>
                </div>
              </div>
        ) 
    }else if(location.pathname=='/signup'){
      return (
        <div className={styles.outmain}>
                <div className={styles.spin}>
                <Signup {...loginProps}/>
                </div>
              </div>
        ) 
    }else{
      return (
        <div className={styles.outmain}>
                <div className={styles.spin}>
                  {children}
                </div>
              </div>
        ) 
    }
    
  }
}

App.propTypes = {
  //children: PropTypes.element.isRequired,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  loginButtonLoading: PropTypes.bool,
  login: PropTypes.bool,
  lock: PropTypes.bool,
  SignUp: PropTypes.bool,
  user: PropTypes.object,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool
}

export default connect(({app}) => ({app}))(App)
