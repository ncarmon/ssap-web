import React, {PropTypes} from 'react'
import {
    Search,
    Button,
    Modal,
    LocaleProvider,
    Input,
    List,
    Row, Col, message,AutoComplete,InputNumber
} from 'antd'
const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;
import {request} from '../../utils'
import {Card, Image, Dimmer, Loader, Segment} from 'semantic-ui-react';
import GoogleMap from 'google-map-react';
import PickerStyle from './ContentPicker.less';
import ShowDate from './ShowDate';
class Summary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screens_contents: props.value || [],
            contents:[]
        };
        this.state.screens_contents.forEach((screen)=>{
            screen.availability.forEach((dates)=>{
                dates.contents=[];
                dates.available=dates.available_pulses;
                this.addContentRow(dates);
            });
        });
    }
    componentDidMount() {
    }

    componentWillUnmount() {
    }

    searchContents(keyword) {
        redius = redius || 10000;
        let me = this;
        me.updateSearchStatus(!hideLoading);
        request('screens/nearby-screens', {
            data: {
                "lat": lat,
                "long": lng,
                "radius_meters": redius
            }
        }, function (err, data) {
            me.updateSearchStatus(false);
            if (data) {
                me.mergeScreenResult(data);
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.setState({...this.state,screens_contents:value})
        }
    }
    triggerChange = (changedValue) => {
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(changedValue);
        }
    }
    handleContentSearch=(keyword)=>{
        let me=this;
        request('content/list',{data:{field:'file_name',keyword:keyword}},function (err,res) {
            if(res){
                me.setState({...me.state,contents:res.list})
            }
        })
    }
    render() {
        const {

        } = this.props;
        const ContentOptions = this.state.contents.map((content,key) => (
            <Option key={content._id+key} value={content._id} lbl={content.file_name}>
                <Row>
                    <Col span={5}>
                        <div className={PickerStyle.content_search_avatar} style={{'backgroundImage':'url('+content.url+')'}}></div>
                    </Col >
                    <Col span={15} className={PickerStyle.content_search_lbl} >{content.file_name}</Col>

                </Row>
            </Option>
        ));
        return <Row style={{marginTop: '10px'}}>
            <Col span={24}>
                <List itemLayout="vertical" size="large"
                      dataSource={this.state.screens_contents}
                      renderItem={screen => (
                          <List.Item key={'result_content' + screen.screen_id}>
                              <Card fluid>
                                  <Card.Content>
                                      <Image floated='right' size='mini' src={screen.url}/>
                                      <Card.Header>
                                          {screen.resolution}
                                      </Card.Header>
                                      <Card.Meta>
                                          {screen.address}
                                      </Card.Meta>
                                      <Card.Description>
                                          {screen.availability.map((dates, key) =>
                                              <Row key={'dtrange'+key}>
                                                  <Col span={5}>
                                                      <div>
                                                          <ShowDate date={dates.start_date} /> to
                                                          <ShowDate date={dates.end_date} />
                                                      </div>
                                                      <div><i>Total {dates.available_pulses} pulses available</i></div>
                                                  </Col>
                                                  <Col span={19}>
                                                      {dates.contents.map((contentRow,key)=>
                                                              <Row key={'contentRow'+key} style={{    borderBottom: 'solid 1px #f5f5f5'}}>
                                                                  <Col span={10}>
                                                                      <AutoComplete
                                                                          className="certain-category-search"
                                                                          dropdownClassName="certain-category-search-dropdown"
                                                                          dropdownMatchSelectWidth={false}
                                                                          dropdownStyle={{ width: 300 }}
                                                                          style={{ width: '80%' }}
                                                                          placeholder="Search Content"
                                                                          optionLabelProp="lbl"
                                                                          onSelect={(val)=>{
                                                                              contentRow.content=val;
                                                                              this.updateState();
                                                                          }}
                                                                          onSearch={(keyword)=>{this.handleContentSearch(keyword)}}
                                                                          dataSource={ContentOptions}
                                                                      />
                                                                  </Col>
                                                                  <Col span={5}>
                                                                      <InputNumber
                                                                          defaultValue={1}
                                                                          min={1}
                                                                          max={contentRow.remaining}
                                                                          formatter={value => `${value} pulse`}
                                                                          parser={value => value.replace('pulse', '')}
                                                                          onChange={(val)=>{
                                                                              this.updatePulseCount(dates,contentRow,val);
                                                                              this.updateState();
                                                                          }}
                                                                      />
                                                                  </Col>
                                                                  <Col span={5}>
                                                                      {(dates.contents.length-1)==key&&dates.available>0?<Button type="primary" shape="circle" icon="plus" onClick={()=>{
                                                                          this.addContentRow(dates);
                                                                          this.updateState();
                                                                      }} />:''}
                                                                      {
                                                                          (dates.contents.length-1)!=key?<Button type="danger" shape="circle" icon="minus" onClick={()=>{
                                                                              this.removeContentRow(dates,key);
                                                                              this.updateState();
                                                                          }} />:''
                                                                      }
                                                                  </Col>
                                                              </Row>
                                                          )}
                                                  </Col>
                                              </Row>
                                          )}
                                      </Card.Description>
                                  </Card.Content>
                              </Card>
                          </List.Item>
                      )}
                />
            </Col>
        </Row>
    }
}

export default Summary