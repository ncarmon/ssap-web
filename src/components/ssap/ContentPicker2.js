import React, {PropTypes} from 'react'
import {
    Search,
    Button,
    Modal,
    LocaleProvider,
    Input,
    List,
    Row, Col, message,AutoComplete,InputNumber,Icon,Tooltip
} from 'antd'
const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;
import moment from 'moment';
import {request,config,lang} from '../../utils'
import {Card, Image, Dimmer, Loader, Segment,Label} from 'semantic-ui-react';
import GoogleMap from 'google-map-react';
import PickerStyle from './ContentPicker.less';
import ShowDate from './ShowDate';
class ContentPicker2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contents:[],
            campaign_contents: props.value || [{pulse:1}]
        };
    }
    componentDidMount() {
        this.handleContentSearch('');
    }
    componentWillUnmount() {
    }


    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value || [{pulse:1}];
            this.setState({...this.state,campaign_contents:value})
        }
    }
    triggerChange = (changedValue) => {
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(changedValue);
        }
    }
    handleContentSearch=(keyword)=>{
        let me=this;
        let contentsAllowed=[1];
        let today=moment();
        let nextStartDate=moment();
        nextStartDate.add(1, 'weeks').isoWeekday(config.campaignStartDates[0]);
        if(!(today.day()>config.cutoffDay&&this.props.startDate<=nextStartDate)){
            contentsAllowed.push(-1);
        }
        request('content/list',{data:{field:'file_name',keyword:keyword,filter:JSON.stringify({act1:contentsAllowed})}},function (err,res) {
            if(res){
                me.setState({...me.state,contents:res.list})
            }
        })
    }
    render() {
        const {

        } = this.props;
        const ContentOptions = this.state.contents.map((content,key) => (
            <Option key={content._id+key} value={JSON.stringify(content)} lbl={content.file_name}>
                <Row>
                    <Col span={5}>
                        <div className={PickerStyle.content_search_avatar} style={{'backgroundImage':'url('+content.thumb+')'}}>
                            
                        </div>
                    </Col >
                    <Col span={15} offset={1}  className={PickerStyle.content_search_lbl} >
                        {
                            content.act1==1&&
                            <Tooltip title={lang.tr("Approved")}><Icon type="check-circle" style={{color:'green'}} /></Tooltip>
                        }
                        {
                            content.act1==-1&&
                            <Tooltip title={lang.tr("Pending")}><Icon type="clock-circle-o" style={{color:'orange'}} /></Tooltip>
                        }
                        {content.file_name}
                    </Col>

                </Row>
            </Option>
        ));
        return <Row style={{marginTop: '10px'}}>
            <Col span={24}>
            {
                this.state.campaign_contents.map((campaign_content,key)=>
                <Row key={key} style={{    borderBottom: 'solid 1px #f5f5f5'}}>
                <Col span={12}>
                {
                (campaign_content&&campaign_content.content)?
                <Segment padded>
                    <Label disabled={this.props.disabled} attached='bottom' size="mini" onClick={()=>{
                        if(!this.props.disabled){
                            campaign_content.content=null;
                            this.setState({...this.state});
                        }
                    }} style={{textAlign:'center'}} >{lang.tr('Change')}</Label>
                    <div className={PickerStyle.content_search_avatar_fixed} style={{'backgroundImage':'url('+campaign_content.content.thumb+')'}}></div>
                </Segment>:
                <AutoComplete
                    disabled={this.props.disabled}
                    className="certain-category-search"
                    dropdownClassName="certain-category-search-dropdown"
                    dropdownMatchSelectWidth={false}
                    dropdownStyle={{ width: 320 }}
                    style={{ width: '90%' }}
                    placeholder={lang.tr("Search Content")}
                    optionLabelProp="lbl"
                    onSelect={(val)=>{
                        campaign_content.content=JSON.parse(val);
                        this.setState({...this.state});
                        this.triggerChange(this.state.campaign_contents);
                    }}
                    onSearch={(keyword)=>{this.handleContentSearch(keyword)}}
                    dataSource={ContentOptions}
                />

                }
                </Col>
                <Col span={5}>
                    <InputNumber
                    disabled={this.props.disabled}
                    defaultValue={campaign_content.pulse || 1}
                    min={1}
                    max={config.max_pulses}
                    formatter={value => value+' '+lang.tr('pulse')}
                    parser={value => value.replace(lang.tr('pulse'), '')}
                    onChange={(val)=>{
                        campaign_content.pulse=val;
                        this.setState({...this.state});
                        this.triggerChange(this.state.campaign_contents);
                    }}
                    />
                </Col>
                <Col span={3}>
                    {
                        key==(this.state.campaign_contents.length-1)&&
                        <Button type="primary" shape="circle" icon="plus" onClick={()=>{
                            this.state.campaign_contents.push({pulse:1})
                            this.setState({...this.state});
                        }} />
                    }
                    {
                        (key!=0&&key==(this.state.campaign_contents.length-1))&&
                        <Button type="danger" shape="circle" icon="minus" onClick={()=>{
                            this.state.campaign_contents.splice(key,1)
                            this.setState({...this.state});
                        }} />
                    }
                </Col>
                </Row>

                 )
            }
            
            </Col>
        </Row>
    }
}

export default ContentPicker2