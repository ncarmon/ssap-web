import React, {PropTypes} from 'react'
import {
    Search,
    Button,
    Modal,
    LocaleProvider,
    Input,
    List,
    Row, Col, message,AutoComplete,InputNumber
} from 'antd'
const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;
import {request} from '../../utils'
import {Card, Image, Dimmer, Loader, Segment,Label} from 'semantic-ui-react';
import GoogleMap from 'google-map-react';
import PickerStyle from './ContentPicker.less';
import ShowDate from './ShowDate';
class ContentPicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screens_contents: props.value || [],
            contents:[]
        };
        this.state.screens_contents.forEach((screen)=>{
            screen.availability.forEach((dates)=>{
                dates.contents=dates.contents || [];
                dates.available=dates.available_pulses;
                if(!dates.contents.length){
                    this.addContentRow(dates);
                }
                
            });
        });
    }
    updateState=()=>{
        this.setState({...this.state});
        this.triggerChange(this.state.screens_contents);
    };
    addContentRow=(dates)=>{
        dates.contents.push({pulse:1,content:null,remaining:this.getAvailable(dates,1)});
        dates.available--;
    };
    removeContentRow=(dates,index)=>{
        dates.contents.splice(index,1);
        this.recalculateAvailablePulseCount(dates);
    };
    getAvailable=(dates,pulse)=>{
        let usedCount=-1*pulse;
        dates.contents.forEach(row=>{
            usedCount+=parseInt(row.pulse);
        });
        return dates.available_pulses-usedCount;;
    };
    recalculateAvailablePulseCount=(dates)=>{
        dates.contents.forEach(row=>{
            row.remaining=this.getAvailable(dates,row.pulse);
        });
        dates.available=this.getAvailable(dates,0);
    };
    updatePulseCount=(dates,row,count)=>{
        row.pulse=count;
        this.recalculateAvailablePulseCount(dates)
    };


    componentDidMount() {
        this.handleContentSearch('');
    }

    componentWillUnmount() {
    }


    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.setState({...this.state,screens_contents:value})
        }
    }
    triggerChange = (changedValue) => {
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(changedValue);
        }
    }
    handleContentSearch=(keyword)=>{
        let me=this;
        request('content/list',{data:{field:'file_name',keyword:keyword,filter:JSON.stringify({act1:[1]})}},function (err,res) {
            if(res){
                me.setState({...me.state,contents:res.list})
            }
        })
    }
    render() {
        const {

        } = this.props;
        const ContentOptions = this.state.contents.map((content,key) => (
            <Option key={content._id+key} value={JSON.stringify(content)} lbl={content.file_name}>
                <Row>
                    <Col span={5}>
                        <div className={PickerStyle.content_search_avatar} style={{'backgroundImage':'url('+content.thumb+')'}}></div>
                    </Col >
                    <Col span={15} className={PickerStyle.content_search_lbl} >{content.file_name}</Col>

                </Row>
            </Option>
        ));
        return <Row style={{marginTop: '10px'}}>
            {
                this.props.disabled
                &&
                <div style={{width: '100%',
                    'height': '100%',
                    'position': 'absolute',
                    'background': '#00000078',
                    'zIndex': 11}}/>
            }
            <Col span={24}>
                <List itemLayout="vertical" size="large"
                      dataSource={this.state.screens_contents}
                      renderItem={screen => (
                          <List.Item key={'result_content' + screen._id}>
                              <Card fluid>
                                  <Card.Content>
                                      <Image floated='right' size='mini' src={screen.url}/>
                                      <Card.Header>
                                          {screen.resolution}
                                      </Card.Header>
                                      <Card.Meta>
                                          {screen.address}
                                      </Card.Meta>
                                      <Card.Description>
                                          {screen.availability.map((dates, key) =>
                                              <Row key={'dtrange'+key}>
                                                  <Col span={5}>
                                                      <div>
                                                          <ShowDate date={dates.start_date} /> to
                                                          <ShowDate date={dates.end_date} />
                                                      </div>
                                                      <div><i>Total {dates.available_pulses} pulses available</i></div>
                                                  </Col>
                                                  <Col span={19}>
                                                      {dates.contents.map((contentRow,key)=>
                                                              <Row key={'contentRow'+key} style={{    borderBottom: 'solid 1px #f5f5f5'}}>
                                                                  <Col span={12}>
                                                                      {
                                                                          (contentRow.content&&contentRow.content.url)?
                                                                              <Segment padded>
                                                                                  <Label attached='bottom' size="mini" onClick={()=>{
                                                                                      contentRow.content=null;
                                                                                      this.setState({...this.state});
                                                                                  }} style={{textAlign:'center'}} >Change</Label>
                                                                                  <div className={PickerStyle.content_search_avatar} style={{'backgroundImage':'url('+contentRow.content.thumb+')'}}></div>
                                                                              </Segment>:
                                                                              <AutoComplete
                                                                                  className="certain-category-search"
                                                                                  dropdownClassName="certain-category-search-dropdown"
                                                                                  dropdownMatchSelectWidth={false}
                                                                                  dropdownStyle={{ width: 320 }}
                                                                                  style={{ width: '90%' }}
                                                                                  placeholder="Search Content"
                                                                                  optionLabelProp="lbl"
                                                                                  onSelect={(val)=>{
                                                                                      contentRow.content=JSON.parse(val);
                                                                                      this.updateState();
                                                                                  }}
                                                                                  onSearch={(keyword)=>{this.handleContentSearch(keyword)}}
                                                                                  dataSource={ContentOptions}
                                                                              />

                                                                      }
                                                                  </Col>
                                                                  <Col span={5}>
                                                                      <InputNumber
                                                                          defaultValue={contentRow.pulse}
                                                                          min={1}
                                                                          max={contentRow.remaining}
                                                                          formatter={value => `${value} pulse`}
                                                                          parser={value => value.replace('pulse', '')}
                                                                          onChange={(val)=>{
                                                                              this.updatePulseCount(dates,contentRow,val);
                                                                              this.updateState();
                                                                          }}
                                                                      />
                                                                  </Col>
                                                                  <Col span={5}>
                                                                      {(dates.contents.length-1)==key&&dates.available>0?<Button type="primary" shape="circle" icon="plus" onClick={()=>{
                                                                          this.addContentRow(dates);
                                                                          this.updateState();
                                                                      }} />:''}
                                                                      {
                                                                          (dates.contents.length-1)!=key?<Button type="danger" shape="circle" icon="minus" onClick={()=>{
                                                                              this.removeContentRow(dates,key);
                                                                              this.updateState();
                                                                          }} />:''
                                                                      }
                                                                  </Col>
                                                              </Row>
                                                          )}
                                                  </Col>
                                              </Row>
                                          )}
                                      </Card.Description>
                                  </Card.Content>
                              </Card>
                          </List.Item>
                      )}
                />
            </Col>
        </Row>
    }
}

export default ContentPicker